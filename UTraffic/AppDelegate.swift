//
//  AppDelegate.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 9/7/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit
import GoogleMaps
import GoogleSignIn
import GooglePlaces
import FBSDKCoreKit
import FBSDKLoginKit
import FacebookCore
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift
import UserNotifications
import DCKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    var window: UIWindow?
    var profanityList_EN = [String]()
    var profanityList_VN = [String]()
    var orientationLock = UIInterfaceOrientationMask.all
    func registerForPushNotifications() {
      UNUserNotificationCenter.current()
        .requestAuthorization(options: [.alert, .sound, .badge]) {
          [weak self] granted, error in
            
          print("Permission granted: \(granted)")
          guard granted else { return }
          self?.getNotificationSettings()
      }
    }
    func getNotificationSettings() {
      UNUserNotificationCenter.current().getNotificationSettings { settings in
        print("Notification settings: \(settings)")
        guard settings.authorizationStatus == .authorized else { return }
        DispatchQueue.main.async {
          UIApplication.shared.registerForRemoteNotifications()
        }
      }
    }
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        do {
            let path: String =  Bundle.main.path(forResource: "EN-ProfanityList", ofType: "txt")!
            let file = try String(contentsOfFile: path)
            profanityList_EN = file.components(separatedBy: "\r\n")
            profanityList_EN.removeLast()
            profanityList_EN = profanityList_EN.removeDuplicates()

        } catch let error {
            print("Fatal Error: \(error.localizedDescription)")
        }
        
        do {
            let path: String =  Bundle.main.path(forResource: "VN-ProfanityList", ofType: "txt")!
            let file = try String(contentsOfFile: path)
            profanityList_VN = file.components(separatedBy: "\n")
            profanityList_VN.removeLast()
            profanityList_VN = profanityList_VN.removeDuplicates()
        } catch let error {
            print("Fatal Error: \(error.localizedDescription)")
        }

        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //Google Map API
        IQKeyboardManager.shared.enable = true
    
        GMSServices.provideAPIKey("AIzaSyBy9V-ZUsws7Gf1BoYDacA_RCFq5aUMtLQ")
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        GIDSignIn.sharedInstance().clientID = "240472685612-cb39po6he2i51girbbsdinjmmltpt3no.apps.googleusercontent.com"
        GMSPlacesClient.provideAPIKey("AIzaSyBy9V-ZUsws7Gf1BoYDacA_RCFq5aUMtLQ")
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)

               application.registerUserNotificationSettings(settings)
               application.registerForRemoteNotifications()
        return true
        
    }
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
      let token = tokenParts.joined()
        UserDefaults.standard.set(token, forKey: "deviceToken")
      print("Device Token: \(token)")
        
    }


    func application(_ application: UIApplication,didFailToRegisterForRemoteNotificationsWithError error: Error) {
      print("Failed to register: \(error)")
    }
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url)
        let facebookDidHandle = ApplicationDelegate.shared.application(app, open: url)
      return googleDidHandle || facebookDidHandle
    }
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
            return self.orientationLock
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }



}

