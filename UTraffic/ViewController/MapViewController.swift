//
//  MapViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 9/7/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import TextFieldEffects
import AVFoundation
import Alamofire
import SwiftyJSON
import MaterialComponents.MaterialSnackbar
import MBProgressHUD
import MediaPlayer
class MapViewController: UIViewController,GMSMapViewDelegate {
    
    @IBOutlet weak var btnSideMenu: RoundButton!
    @IBOutlet weak var btnReCentre: RoundButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var sideMenuLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var sideMenuTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchFormTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var sideMenu: UIView!
    @IBOutlet weak var btnReport: RoundButton!
    @IBOutlet weak var reportForm: UIView!
    @IBOutlet weak var btnDirection: RoundButton!
    @IBOutlet weak var searchDirectionMenu: UIView!
    @IBOutlet weak var btnShowReports: RoundButton!
    @IBOutlet weak var musicContainer: UIView!
    @IBOutlet weak var searchLocationContainer: UIView!
    @IBOutlet weak var weatherContainer: UIView!
    @IBOutlet weak var liveTrafficContainer: UIView!
    
    @IBAction func unwindToThisVC(segue: UIStoryboardSegue) { }
    
    @IBOutlet weak var reportFormBottomConstraint: NSLayoutConstraint!

    //Location
    let locationManager = CLLocationManager()
    var location: CLLocation?
    var lastLocationError: Error?

    let menuBlackView = UIView()
    let animationDuration = 0.2
    var currentSpeed: Int = 0
    
    var sideMenuWidth = CGFloat()
    var reportFormHeight = CGFloat()
    var searchFormHeight = CGFloat()
    var musicContainerWidth = CGFloat()

    var menuShowing = Bool()
    var reportShowing = Bool()
    var searchShowing = Bool()
    var musicShowing = Bool()
    var lastLat = Double()
    var lastLong = Double()
    var currentLat = Double()
    var currentLong = Double()
    var markerCount = Int()
    var beginGetDirection = Bool()
    var beginGetLocation = Bool()
    let marker1 = GMSMarker()
    let marker2 = GMSMarker()
    let directionLine = GMSPolyline()
    let routePolyline1 = GMSPolyline()
    let routePolyline2 = GMSPolyline()
    var ratingMode = Bool()
    var dateInterval = Int()
    var mvc = MusicPlayerViewController()
    var rvc = ReportViewController()
    var reportsId:[String: [[String]]] = [:]
    var statusTimer = Timer()
    var reportsTimer = Timer()
    var evaluationMode = Bool()
    let startLocationMarker = GMSMarker()
    let endLocationMarker = GMSMarker()
    let routeLing = GMSPolyline()
    var oldStatusPolylines = [GMSPolyline]()
    var oldStatusPolylineColors = [UIColor]()
    var oldReportMarkers = [GMSMarker]()
    var hasServerError = Bool()
    var isFirstLaunch = Bool()
    let placeMarker1 = GMSMarker()
    let placeMarker2 = GMSMarker()
    var isGettingStatus = Bool()
    
    @IBOutlet weak var setMarkerView: UIRoundView!
    @IBOutlet weak var instructionTextView: UITextView!
    @IBOutlet weak var btnRestartSequence: RoundButton!
    @IBOutlet weak var btnConfirmDirection: RoundButton!
    @IBOutlet weak var musicContainerLeadingConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        isGettingStatus = false
        isFirstLaunch = true
        mvc = children[2] as! MusicPlayerViewController
        rvc = children[5] as! ReportViewController
        ratingMode = false
        //btnShowReports.isHidden = true

        getTimeReference(){(isSuccess,message,interval) in
            var timeInterval = 180000
            if isSuccess == true  && (interval != nil || interval != 0) {
                timeInterval = interval!
            }
            UserDefaults.standard.set(timeInterval, forKey: "timeInterval")
            print("Current time interval:", timeInterval)
        }

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        reportForm.addGestureRecognizer(tapGesture)
        
        sideMenuWidth = sideMenu.frame.width
        reportFormHeight = reportForm.frame.height
        searchFormHeight = searchDirectionMenu.frame.height
        musicContainerWidth = musicContainer.frame.width

        //Request for location
        checkLocationServices()
        menuShowing = false
        reportShowing = false
        searchShowing = false
        musicShowing = false
        

        //setup transparent view
        menuBlackView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        menuBlackView.frame = self.view.frame
        self.view.insertSubview(menuBlackView, belowSubview: sideMenu)
        menuBlackView.alpha=0
        menuBlackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))

        self.mapView.delegate = self
        
        currentLat = 0
        currentLong = 0
        beginGetDirection = false
        markerCount=1
        setMarkerView.isHidden = true
        instructionTextView.isScrollEnabled = false
        hasServerError = false
        
        placeMarker1.icon = GMSMarker.markerImage(with: .red)
        placeMarker2.icon = GMSMarker.markerImage(with: .blue)
        

    }
    override func viewDidAppear(_ animated: Bool) {
        evaluationMode = UserDefaults.standard.bool(forKey: "evaluationMode")
        //getNearTrafficStatus()
        if evaluationMode == true{
            getTrafficReports()
            restartReportsTimer()
        }
        else {
            reportsTimer.invalidate()
        }
        self.reCentreUserLocation(self)
    }

    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        reportForm.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        checkLocationServices()
        liveTrafficContainer.isHidden = true
        btnShowReports.isHidden = true
        if menuShowing == true{
            sideMenuLeadingConstraint.constant = 0
        }
        else{
            sideMenuLeadingConstraint.constant = -sideMenuWidth
        }
        if reportShowing == true {
            reportFormBottomConstraint.constant = 0
        }
        else {
            reportFormBottomConstraint.constant = -reportFormHeight
        }
        if searchShowing == true{
            searchFormTopConstraint.constant = 0
        }
        else{
            searchFormTopConstraint.constant = -searchFormHeight
        }
        if musicShowing == true {
            musicContainerLeadingConstraint.constant = 0
        }
        else {
            musicContainerLeadingConstraint.constant = -musicContainerWidth + 30
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    

    @IBAction func openSearchDirectionMenu(_ sender: Any) {
        let CVC = children[6] as! SearchDirectionViewController
        directionLine.map = nil
        routePolyline1.map = nil
        routePolyline2.map = nil
        marker1.map = nil
        marker2.map = nil
        placeMarker1.map = nil
        placeMarker2.map = nil
        CVC.isSearching = false
        showSearchDirectionMenu()
    }
    func showSearchDirectionMenu(){
        if searchShowing == false {
            dismissMusicContainer()
            searchFormTopConstraint.constant = 0

            UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 1,initialSpringVelocity: 2,options: .curveEaseOut,animations: {
                self.view.superview?.layoutIfNeeded()
            },completion: nil)
            searchShowing = !searchShowing
        }
        else{
            searchFormTopConstraint.constant = -searchFormHeight
                UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 1,initialSpringVelocity: 2,options: .curveEaseOut,animations: {
                    self.view.superview?.layoutIfNeeded()
                },completion: nil)
            searchShowing = !searchShowing
        }
    }
    // MARK: - Open Menu
    @IBAction func openMenu(_ sender: Any) {
       if searchShowing == true {
           showSearchDirectionMenu()
       }
        dismissMusicContainer()
        sideMenuLeadingConstraint.constant = 0
        UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 1,initialSpringVelocity: 1,options: .curveEaseInOut,animations: {
            self.menuBlackView.alpha=1
            self.btnSideMenu.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
        self.view.superview?.layoutIfNeeded()
        },completion: nil)
        menuShowing = !menuShowing
    }
    func openMusicContainer(button: UIButton){
        musicContainerLeadingConstraint.constant = 0
        UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 1,initialSpringVelocity: 1,options: .curveEaseInOut,animations: {
            button.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            self.view.superview?.layoutIfNeeded()
        },completion: nil)
        musicShowing = true
    }
    func closeMusicContainer(button:UIButton){
        musicContainerLeadingConstraint.constant = -musicContainerWidth + 30
        UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 1,initialSpringVelocity: 1,options: .curveEaseInOut,animations: {
            button.transform = CGAffineTransform.identity
            self.view.superview?.layoutIfNeeded()
        },completion: nil)
        musicShowing = false
    }
    func dismissMusicContainer(){
        closeMusicContainer(button: mvc.openButton)
    }
    
    // MARK: - Open Report form
    @IBAction func reportTraffic(_ sender: Any) {
        dismissMusicContainer()
        openReportMenu()
        if searchShowing == true {
            showSearchDirectionMenu()
        }
        
    }
    func setReportMarker(Lat: CLLocationDegrees,Long: CLLocationDegrees, Title: String?, Snippet: String?,Color:UIColor,map:GMSMapView){
        
        let marker = GMSMarker()
        var markerImage = UIImage()
        marker.position = CLLocationCoordinate2D(latitude: Lat, longitude: Long)
        if #available(iOS 13.0, *) {
            let configuration = UIImage.SymbolConfiguration(scale: .large)
            markerImage = UIImage(systemName: "mappin.and.ellipse",withConfiguration: configuration)!
        } else {
            markerImage = UIImage(named: "mappin.and.ellipse")!
        }
        markerImage.withRenderingMode(.alwaysTemplate)
        let imageV1 = UIImageView(image: markerImage)
        imageV1.tintColor = Color
        imageV1.tintAdjustmentMode = .normal
        marker.iconView = imageV1
        marker.title = Title
        marker.snippet = Snippet
        marker.map = map
        oldReportMarkers.append(marker)

    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    //do what ever you want
        print("marker clicked")
        if marker.title == "Report"{
            let array = marker.snippet!.components(separatedBy: ",")
            print(array)
            let rdVC : ReportDetailViewController = UIStoryboard(name: "ReportDetail", bundle: nil).instantiateViewController(withIdentifier: "ReportDetailViewController") as! ReportDetailViewController
            rdVC.ids = array
            self.present(rdVC, animated: true, completion: nil)
        }
        return true
    }
    func mapView(_ mapView: GMSMapView, didCloseInfoWindowOf marker: GMSMarker) {
        
    }
    func openReportMenu(){
        let speed = String(currentSpeed)
        rvc.changeCurrentSpeed(speed: speed)
        reportFormBottomConstraint.constant = 0
        UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 1,initialSpringVelocity: 2,options: .curveEaseOut,animations: {
            self.menuBlackView.alpha=1
            self.view.superview?.layoutIfNeeded()
        },completion: nil)
        reportShowing = !reportShowing
    }
    // MARK: - Handle dismiss
    @objc func handleDismiss() {
        if menuShowing == true {
            sideMenuLeadingConstraint.constant = -sideMenuWidth
            UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 1,initialSpringVelocity: 2,options: .curveEaseOut,animations: {
                self.menuBlackView.alpha=0
                self.btnSideMenu.transform = CGAffineTransform.identity
                self.view.superview?.layoutIfNeeded()
            },completion: nil)
            menuShowing = !menuShowing
        }
        if reportShowing == true{
            closeReportMenu()
        }
        for x in oldReportMarkers{
            x.map = mapView
        }
        for x in oldStatusPolylines{
            x.map = mapView
        }
        self.view.endEditing(true)
        print("dismiss")
    }
    func closeReportMenu(){
                reportFormBottomConstraint.constant = -reportFormHeight
            UIView.animate(withDuration: animationDuration, delay: 0, usingSpringWithDamping: 1,initialSpringVelocity: 2,options: .curveEaseOut,animations: {
                self.menuBlackView.alpha=0
                
                self.view.superview?.layoutIfNeeded()
            },completion: nil)
                reportShowing = !reportShowing
    }

    @IBAction func reCentreUserLocation(_ sender: Any) {
        dismissMusicContainer()
        reCenter(zoom: 18)
        getNearTrafficStatus(searchlat: nil, searchlong: nil)
    }
    func reCenter(zoom:Float){
        if let location = locationManager.location {
            mapView.animate(toLocation: location.coordinate)
            mapView.animate(toZoom: zoom)
        }
        restartStatusTimer()
    }
    
    // MARK: - Set up Location Manager
    func setUpLocationManager(){
        locationManager.delegate=self
        locationManager.desiredAccuracy=kCLLocationAccuracyBestForNavigation
    }
    
    func checkLocationServices(){
        if CLLocationManager.locationServicesEnabled(){
            setUpLocationManager()
            checkLocationAuthorization()
        }
    }
    
    //Ask for user permission to get their location
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestAlwaysAuthorization()
            break
        case .restricted, .denied:
            // Disable location features
            reportLocationServicesDeniedError()
            break
        case .authorizedWhenInUse:

            // Enable basic location features
            break
        case .authorizedAlways:
            break
        @unknown default:
            fatalError()
        }
    }
    
    func escalateLocationServiceAuthorization() {
        // Escalate only when the authorization is set to when-in-use
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.requestAlwaysAuthorization()
        }
        
    }
    // MARK -
    //TODO: Handle Service Denied event
    func reportLocationServicesDeniedError(){
        //Create an Alert Pop Up
        let alert = UIAlertController(title: "LocationDisableWarningKey".localized, message: "EnableLocationGuidanceKey".localized, preferredStyle: .alert)
        //Create an Ok Button
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        //Add Ok Button we just create to the Alert Pxop Up
        alert.addAction(okAction)
        
        //Show the alert
        present(alert,animated: true,completion: nil)
    }
    
    func mapView(Lat: CLLocationDegrees, Long: CLLocationDegrees, zoom: Float){
        let camera = GMSCameraPosition.camera(withLatitude: Lat, longitude: Long, zoom: zoom)
        let mapView = GMSMapView.map (withFrame: CGRect.zero, camera: camera)
        view = mapView
    }
    
    func setMarker1(Lat: CLLocationDegrees,Long: CLLocationDegrees, Title: String?, Snippet: String?,imageString:String?,map:GMSMapView){
        marker1.map = nil
        marker1.map = map
        var markerImage = UIImage()
        marker1.position = CLLocationCoordinate2D(latitude: Lat, longitude: Long)
        if imageString != nil{
            if #available(iOS 13.0, *) {
                let configuration = UIImage.SymbolConfiguration(scale: .large)
                markerImage = UIImage(systemName: imageString!,withConfiguration: configuration)!
            } else {
                markerImage = UIImage(named: imageString!)!
            }
            markerImage.withRenderingMode(.alwaysTemplate)
            let imageV1 = UIImageView(image: markerImage)
            imageV1.tintColor = UIColor.systemBlue
            imageV1.tintAdjustmentMode = .normal
            marker1.iconView = imageV1
        }
        marker1.title = Title
        marker1.snippet = Snippet
        
        markerCount=2
    }
    func setTestMarker(coordinate:CLLocationCoordinate2D,color:UIColor,map:GMSMapView){
        let marker = GMSMarker()
        marker.position = coordinate
        marker.icon = GMSMarker.markerImage(with: color)
        marker.map = map

    }
    
    func drawPolyline(coordinate1:CLLocationCoordinate2D,coordinate2:CLLocationCoordinate2D,color:UIColor,map:GMSMapView){
        let polyline = GMSPolyline()
        let path = GMSMutablePath()
        path.add(coordinate1)
        path.add(coordinate2)
        polyline.path = path
        polyline.strokeColor = color
        polyline.strokeWidth = 3
        polyline.map = map
    }
    func setMarker2(Lat: CLLocationDegrees,Long: CLLocationDegrees, Title: String?, Snippet: String?,imageString:String,map:GMSMapView){
        var markerImage = UIImage()
        marker2.map = nil
        marker2.map = map
        marker2.position = CLLocationCoordinate2D(latitude: Lat, longitude: Long)
        if #available(iOS 13.0, *) {
            let configuration = UIImage.SymbolConfiguration(scale: .large)
            markerImage = UIImage(systemName: imageString,withConfiguration: configuration)!
        } else {
            markerImage = UIImage(named: imageString)!
        }
        markerImage.withRenderingMode(.alwaysTemplate)
        markerImage.withRenderingMode(.alwaysTemplate)
        let imageV2 = UIImageView(image: markerImage)
        imageV2.tintColor = UIColor.systemRed
        imageV2.tintAdjustmentMode = .normal
        marker2.iconView = imageV2
        marker2.title = Title
        marker2.snippet = Snippet
        
        markerCount=3
    }
    func resetMarkers(){
        markerCount = 1
        marker1.map = nil
        marker2.map = nil
        directionLine.map = nil
        placeMarker1.map = nil
        placeMarker2.map = nil
    }
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        if beginGetDirection == false && beginGetLocation == true{
            if markerCount == 1 {
                setMarker1(Lat: coordinate.latitude, Long: coordinate.longitude, Title: nil, Snippet: nil, imageString: "mappin", map: mapView)
                instructionTextView.text = "SetMarker2Key".localized
            }
            else if markerCount == 2{
                setMarker2(Lat: coordinate.latitude, Long: coordinate.longitude, Title: nil, Snippet: nil, imageString: "mappin", map: mapView)
                drawDirectionPolyline(startLat: marker1.position.latitude, startLong: marker1.position.longitude, endLat: marker2.position.latitude, endLong: marker2.position.longitude,map: mapView)
                enableConfirmDirectionButton()
                instructionTextView.text = "ConfirmDirectionKey".localized
            }
        }
        if beginGetLocation == false && beginGetDirection == true{
            print("direction")
            if markerCount < 3{
                setMarker2(Lat: coordinate.latitude, Long: coordinate.longitude, Title: nil, Snippet: nil, imageString: "mappin", map: mapView)
                drawDirectionPolyline(startLat: marker1.position.latitude, startLong: marker1.position.longitude, endLat: marker2.position.latitude, endLong: marker2.position.longitude,map: mapView)
                enableConfirmDirectionButton()
                instructionTextView.text = "ConfirmDirectionKey".localized
            }
        }

    }
//    func confirmDirectionAfterTwoMarker(){
//        var message = String()
//        let CVC = children[5] as! ReportViewController
//        var err = false
//        if beginGetLocation == true && beginGetDirection == false{
//            message = "ConfirmLocationKey".localized
//        }
//        else if beginGetLocation == false && beginGetDirection == true{
//            message = "ConfirmDirectionKey".localized
//        }
//        let alert = UIAlertController(title: "ConfirmKey".localized, message: message, preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "YesKey".localized, style: .default , handler:{ (UIAlertAction)in
//            if self.beginGetLocation == false && self.beginGetDirection == true{
//                //m1Lat = self.snapToRoad(lat: self.currentLat, long: self.currentLong).latitude!
//                snapToRoad(lat: self.currentLat, long: self.currentLong){(suc,lat,long) in
//                    if suc {
//                        print(lat!,long!)
//                        CVC.curLat=lat!
//                        CVC.curLong=long!
//                    }
//                    else{
//                        err = true
//
//                    }
//
//                }
//            }
//            else if self.beginGetLocation == true && self.beginGetDirection == false{
//                snapToRoad(lat: self.marker1.position.latitude,long: self.marker1.position.longitude){(suc,lat,long) in
//                    if suc {
//                        print(lat!,long!)
//                        CVC.curLat=lat!
//                        CVC.curLong=long!
//                    }
//                    else{
//                        err = true
//                        CVC.curLat=0
//                        CVC.curLong=0
//                    }
//
//                }
//            }
//
//            snapToRoad(lat: self.marker2.position.latitude,long: self.marker2.position.longitude){(suc,lat,long) in
//                if suc {
//                    print(lat!,long!)
//                    CVC.refLat = lat!
//                    CVC.refLong = long!
//                }
//                else{
//                    err = true
//                    CVC.refLat = 0
//                    CVC.refLong = 0
//                }
//                self.beginGetDirection = false
//                self.beginGetLocation = false
//                self.resetMarkers()
//                self.openReportMenu()
//                if err == true {
//                    CVC.displayAlert(title: "ErrorKey".localized, message: "ServerErrorKey".localized)
//                }
//            }
//           }))
//
//        alert.addAction(UIAlertAction(title: "NoKey".localized, style: .default , handler:{ (UIAlertAction)in
//            self.setDirectionMarker()
//           }))
//           self.present(alert, animated: true, completion: {
//               print("completion block")
//           })
//    }

    func setDirectionMarker(){
        self.resetMarkers()
        if beginGetLocation == true && beginGetDirection == false{
            instructionTextView.text = "SetMarker1Key".localized
        }
        else if beginGetLocation == false && beginGetDirection == true{
            instructionTextView.text = "SetDirectionMarkerKey".localized
        }
        setMarkerView.isHidden = false
        btnConfirmDirection.backgroundColor = .gray
        btnConfirmDirection.isEnabled = false
    }
    func hideUI(){
        searchLocationContainer.isHidden = true
        weatherContainer.isHidden = true
        musicContainer.isHidden = true
        liveTrafficContainer.isHidden = true
        sideMenu.isHidden = true
        reportForm.isHidden = true
        searchDirectionMenu.isHidden = true
    }
    func showUI(){
        searchLocationContainer.isHidden = false
        weatherContainer.isHidden = false
        musicContainer.isHidden = false
        liveTrafficContainer.isHidden = true
        sideMenu.isHidden = false
        reportForm.isHidden = false
        searchDirectionMenu.isHidden = false
    }
    @IBAction func closeSetMarkerView(_ sender: Any) {
        resetMarkers()
        setMarkerView.isHidden = true
        showUI()
        openReportMenu()
    }
    @IBAction func confirmDirection(_ sender: Any) {
        let CVC = children[5] as! ReportViewController
        let dispatchGroup = DispatchGroup()
        var err = false
        showIndicator(text: "LoadingKey".localized, view: self.view)
        dispatchGroup.enter()
        snapToRoad(lat: self.marker1.layer.latitude, long: self.marker1.layer.longitude){(suc,lat,long) in
        if suc {
            CVC.curLat=lat!
            CVC.curLong=long!
            snapToRoad(lat: self.marker2.position.latitude,long: self.marker2.position.longitude){(succ,latt,longg) in
                if succ {
                 CVC.refLat = latt!
                 CVC.refLong = longg!
                }
                else{
                 err = true
                 CVC.refLat = 0
                 CVC.refLong = 0
                }
            }
        }
        else{
            err = true
            CVC.curLat=0
            CVC.curLong=0
        }
        dispatchGroup.leave()
       }
        dispatchGroup.notify(queue: .main){
            hideIndicator(view: self.view)
            self.beginGetDirection = false
            self.beginGetLocation = false
            self.resetMarkers()
            self.showUI()
            self.setMarkerView.isHidden = true
            self.openReportMenu()
            if err == true{
                CVC.displayAlert(title: "ErrorKey".localized, message: "ServerErrorKey".localized)
            }
        }
    }
    
    func currentDirection(){
        self.resetMarkers()
        let sLat = lastLat
        let sLong = lastLong
        let eLat = currentLat
        let eLong = currentLong
        setMarker1(Lat: sLat, Long: sLong, Title: nil, Snippet: nil, imageString: "mappin", map: mapView)
        setMarker2(Lat: eLat, Long: eLong, Title: nil, Snippet: nil, imageString: "mappin", map: mapView)
        drawDirectionPolyline(startLat: sLat, startLong: sLong, endLat: eLat, endLong: eLong,map: mapView)
        instructionTextView.text = "ConfirmDirectionKey".localized
        enableConfirmDirectionButton()
        setMarkerView.isHidden = false

//        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
//            self.confirmDirectionAfterTwoMarker()
//        })
    }
    @IBAction func restartSetMarkerSequence(_ sender: Any) {
        resetMarkers()
        disableConfirmDirectionButton()
        if beginGetDirection == false && beginGetLocation == true{
            self.markerCount = 1
            instructionTextView.text = "SetDirectionMarkerKey".localized
            
        }
        else if beginGetDirection == true && beginGetLocation == false{
            self.markerCount = 2
            setMarker1(Lat: self.currentLat, Long: self.currentLong, Title: nil, Snippet: nil, imageString: "mappin", map: mapView)
            instructionTextView.text = "SetMarker1Key".localized
        }
    }
    func enableConfirmDirectionButton(){
        btnConfirmDirection.backgroundColor = UIColor(hexString: "#007AFF")
        btnConfirmDirection.isEnabled = true
    }
    func disableConfirmDirectionButton(){
        btnConfirmDirection.backgroundColor = .gray
        btnConfirmDirection.isEnabled = false
    }
    
    func drawDirectionPolyline(startLat:Double,startLong:Double,endLat:Double,endLong:Double,map:GMSMapView){
        let path = GMSMutablePath()
        path.add(CLLocationCoordinate2D(latitude: startLat, longitude: startLong))
        path.add(CLLocationCoordinate2D(latitude: endLat, longitude: endLong))
        directionLine.path = path
        directionLine.strokeColor = .systemGreen
        directionLine.strokeWidth = 4
        directionLine.map = map
    }
    func drawStatusPolyline (startLat:Double,startLong:Double,endLat:Double,endLong:Double,color:String,map:GMSMapView){
        let path = GMSMutablePath()
        let polyline = GMSPolyline()
        path.add(CLLocationCoordinate2D(latitude: startLat, longitude: startLong))
        path.add(CLLocationCoordinate2D(latitude: endLat, longitude: endLong))
        polyline.path = path
        polyline.strokeColor = UIColor(hexString: color)
        polyline.strokeWidth = 4
        polyline.map = map
        oldStatusPolylines.append(polyline)
        oldStatusPolylineColors.append(polyline.strokeColor)
    }
    func drawDirection1(coordinateList: [CLLocationCoordinate2D],color:UIColor,map: GMSMapView){
        routePolyline1.map = nil
        let path = GMSMutablePath()
        for x in coordinateList{
            path.add(x)
        }
        let bound = GMSCoordinateBounds(path: path)
        mapView.animate(with: GMSCameraUpdate.fit(bound, withPadding: 30.0))
        routePolyline1.path = path
        routePolyline1.strokeColor = color
        routePolyline1.strokeWidth = 4
        routePolyline1.map = map
    }
    func drawDirection2(coordinateList: [CLLocationCoordinate2D],color:UIColor,map: GMSMapView){
        routePolyline2.map = nil
        let path = GMSMutablePath()
        for x in coordinateList{
            path.add(x)
        }
        let bound = GMSCoordinateBounds(path: path)
        mapView.animate(with: GMSCameraUpdate.fit(bound, withPadding: 30.0))
        routePolyline2.path = path
        routePolyline2.strokeColor = color
        routePolyline2.strokeWidth = 4
        routePolyline2.map = map
    }

    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status != CLAuthorizationStatus.denied{
            locationManager.startUpdatingLocation()
        }
    }

    func startStatusTimer(){
        var timeInterval = Double()
        if UserDefaults.standard.string(forKey: "timeInterval") != nil {
            timeInterval  = Double(UserDefaults.standard.integer(forKey: "timeInterval")/1000)
        }
        else {
            timeInterval = 180
        }
        statusTimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true){_ in
            self.getNearTrafficStatus(searchlat: nil,searchlong: nil)
            print("Start Status Timer")
        }
    }
    func hideStatusPolylines(){
        for x in self.oldStatusPolylines{
            x.strokeColor = .clear
        }
    }
    func showStatusPolylines(){
        for (i,x) in self.oldStatusPolylines.enumerated(){
            x.strokeColor = oldStatusPolylineColors[i]
        }
    }
    func startReportsTimer(){
        var timeInterval = Double()
        if UserDefaults.standard.string(forKey: "timeInterval") != nil {
            timeInterval  = Double(UserDefaults.standard.integer(forKey: "timeInterval")/1000)
        }
        else {
            timeInterval = 180
        }
        reportsTimer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true){_ in
            self.getTrafficReports()
        }
        print("Start Reports Timer")
    }
    func restartReportsTimer(){
        reportsTimer.invalidate()
        startReportsTimer()
    }
    func restartStatusTimer(){
        if isGettingStatus == true {
            return
        }
        print("Restart Status Timer")
        statusTimer.invalidate()
        startStatusTimer()
    }
    @IBAction func showReports(_ sender: Any) {
        print(oldStatusPolylines.count)
    }
    // MARK: Get Traffic Status
    func getNearTrafficStatus(searchlat: Double?, searchlong: Double?){
        if isGettingStatus == true {
            return
        }
        getTimeReference(){(isSuccess,message,interval) in
            var timeInterval = 170000
            if isSuccess == true  && (interval != nil || interval != 0) {
                timeInterval = interval!
            }
            UserDefaults.standard.set(timeInterval, forKey: "timeInterval")
            print("Current time interval:", timeInterval)
        }
        isGettingStatus = true
       print("Current Coordinate for Traffic Status:",currentLat,currentLong)
        if oldStatusPolylines.count != 0{
            for x in oldStatusPolylines {
                x.map = nil
            }
            oldStatusPolylines.removeAll()
        }
        var zoom = Int(self.mapView.camera.zoom)
        if isFirstLaunch == true {
            zoom = 18
            isFirstLaunch = false
        }
        let date = Date()
        let interval = ((date.timeIntervalSince1970)*1000).rounded()
        let dispatchGroup = DispatchGroup()
        var segMentCount = Int()
        let start1 = NSDate()
        var end1 = NSDate()
        var lat = self.currentLat
        var long = self.currentLong
        //zoom = 17
        var time = Int(interval)
        if searchlat != nil || searchlong != nil{
            lat = searchlat!
            long = searchlong!
        }
//
//        time = 1577070075095
        //zoom = 18
                var res = Double(0)
        
        dispatchGroup.enter()
        getCurrentTrafficStatus(lat: lat, lng: long, zoom: zoom, date: time){[weak self](isSuccess,mess,sLat,sLong,eLat,eLong,color,velocity) in
            end1 = NSDate()
            if isSuccess == true{
                for (i,value) in (sLat?.enumerated())!{
                    UserDefaults.standard.set(false, forKey: "hasServerError")
                    let start2 = NSDate()
                    self!.drawStatusPolyline(startLat:sLat![i] , startLong: sLong![i], endLat: eLat![i], endLong: eLong![i], color: color![i], map: self!.mapView)
                    let end2 = NSDate()
                    res = res + end2.timeIntervalSince(start2 as Date)
                }
                segMentCount = sLat!.count
            }
            else if isSuccess == false {
                //if UserDefaults.standard.bool(forKey: "hasServerError") != true{
                let message = MDCSnackbarMessage()
                message.text = mess
                    MDCSnackbarManager.show(message)
//                UserDefaults.standard.set(true, forKey: "hasServerError")
//                }
            }
            dispatchGroup.leave()
        }
        dispatchGroup.notify(queue: .main){
            self.isGettingStatus = false
//            if segMentCount == 0 {
//                let message = MDCSnackbarMessage()
//                message.text = "No Status found"
//                    MDCSnackbarManager.show(message)
//            }
            print("----------------------------------------------------")
            print("Latitude:\(lat)")
            print("Longitude:\(long)")
            print("Zoom level:\(zoom)")
            print("Render Finished","Number of Segment: \(segMentCount)")
            print("Time Interval: \(time)")
            print("Request Time:",end1.timeIntervalSince(start1 as Date))
            print("Render time: \(res)")
            print("----------------------------------------------------")
            print("\n")
            
        }
    }
    func showSnackBar(message: String){
        let mess = MDCSnackbarMessage()
         mess.text = message
         MDCSnackbarManager.show(mess)
    }
    func getTrafficReports(){
        print("Start getting traffic Report")
        for x in oldReportMarkers{
            x.map = nil
        }
        oldReportMarkers.removeAll()
        var end1 = NSDate()
        var start1 = NSDate()
        if let location = locationManager.location {
            let latitute = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            findNearSegment(lat: latitute, lng: longitude){[unowned self](success,mess,idList,sLat,sLong,eLat,eLong) in
                if success == true {
                    if idList == nil{
                        print("Cannot find any near Segment")
                    }
                    else{
                        print("Near Segments:",idList!.joined(separator: ","))
                        getCurrentTrafficReport(lat: latitute, long: longitude,segmentIDs: idList!){[unowned self](isSuccess,message,ids) in
                            var num = 0
                            if isSuccess == true {
                                if !ids.isEmpty{
                                    for (key,value) in ids {
                                        num = num + 1
                                        let reportIdString = value.joined(separator: ",")
                                        self.setReportMarker(Lat: key.latitude, Long: key.longitude, Title: "Report", Snippet: reportIdString, Color: .systemBlue, map: self.mapView)
                                    }
                                }
                                else{
                                    //self.showSnackBar(message: "NoTrafficReportKey".localized)
                                    print("No Reports found")
                                }
                                end1 = NSDate()
                            }
                            else{
                                print("Get traffic Report Error:",message!)
                            }
                            print("----------------------------------------------------")
                            print("Latitude:\(latitute)")
                            print("Longitude:\(longitude)")
                            print("Number of markers:", num)
                            print("Total time:",end1.timeIntervalSince(start1 as Date))
                            print("----------------------------------------------------")
                        }
                    }
                }
                else {
                    print("Get Near Segment for Traffic Report Error:",mess!)
                }
            }
        }
    }
}

// MARK: - CLLocationManagerDelegate
//1
extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse || status == .authorizedAlways else {
            return
        }
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        //mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        guard let speed = manager.location?.speed else { return }
        
        let vel = speed < 0 ? 0 : (speed*3.6).rounded()
        currentSpeed = Int(vel)
//        txtSpeed.text = String(currentSpeed)
//        txtTest.text = String(speed < 0 ? 0 : (speed*3.6).rounded())
        //locationManager.stopUpdatingLocation()
   
        let changedLat = location.coordinate.latitude
        let changedLong = location.coordinate.longitude
        if currentLat == 0 && currentLong == 0{
            currentLat = changedLat
            currentLong = changedLong
        }else
        {
            lastLat = currentLat
            lastLong = currentLong
            //snapToRoad(lat: String(lastLat), long: String(lastLong),elat: String(changedLat),elong: String(changedLong))
            currentLat = changedLat
            currentLong = changedLong
        }
        
    }
}
