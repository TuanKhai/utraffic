//
//  SignUpViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 9/11/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import Photos
import UIKit
import MBProgressHUD
import Alamofire
import SwiftyJSON

class SignUpViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var profileImage: UIRoundImage!
    @IBOutlet var holderView: UIView!
    @IBOutlet weak var txtUsername: DesignableUITextField!
    @IBOutlet weak var txtPassword: DesignableUITextField!
    @IBOutlet weak var txtRePassword: DesignableUITextField!
    @IBOutlet weak var txtName: DesignableUITextField!
    @IBOutlet weak var txtPhoneNumber: DesignableUITextField!
    @IBOutlet weak var txtEmail: DesignableUITextField!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()

    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        holderView.addGestureRecognizer(tapGesture)
        tapGesture.cancelsTouchesInView = false
        // Do any additional setup after loading the view.


    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        txtUsername.placeholder = "UsernameKey".localized
        txtPassword.placeholder = "PasswordKey".localized
        txtRePassword.placeholder = "ConfirmPasswordKey".localized
        txtName.placeholder = "FullNameKey".localized
        txtPhoneNumber.placeholder = "PhoneNumberKey".localized
        txtEmail.placeholder = "EmailKey".localized
        btnSignUp.setTitle("SignUpKey".localized, for: .normal)
        btnEdit.setTitle("EditKey".localized, for: .normal)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }

    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        holderView.endEditing(true)
    }
    @IBAction func editProfilePicture(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "TakePictureKey".localized, style: .default , handler:{ (UIAlertAction)in
            print("Camera")
            self.getImage(fromSourceType: .camera)
        }))

        alert.addAction(UIAlertAction(title: "ChooseFromLibraryKey".localized, style: .default , handler:{ (UIAlertAction)in
            print("Library")
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "ResetDefaultKey".localized, style: .default , handler:{ (UIAlertAction)in
            print("Remove image")
            resetPicture(imageView: self.profileImage)
            
        }))

        alert.addAction(UIAlertAction(title: "CancelKey".localized, style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImage: UIImage?
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
            self.profileImage.image = selectedImage!
            self.profileImage.clipsToBounds = true
   
            picker.dismiss(animated: true, completion: nil)
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
            self.profileImage.image = selectedImage!
            self.profileImage.clipsToBounds = true
          
            picker.dismiss(animated: true, completion: nil)
        }

    }
    @IBAction func signUp(_ sender: Any) {
        showIndicator(text: "LoadingKey".localized, view: self.view)
        let start = NSDate()
        if NetworkConnection.isConnectedToNetwork() == false {
            displayAlert(title: "ErrorKey".localized, message: "NetworkKey".localized, viewcontroller: self)
            hideIndicator(view: self.view)
            return
        }
        let validation = validate()
        if validation.pass == false{
            displayAlert(title: "ErrorKey".localized, message: validation.message!, viewcontroller: self)
            hideIndicator(view: self.view)
            return
        }
        let username = txtUsername.text!
        let password = txtPassword.text!
        let name = txtName.text!
        let email = txtEmail.text!
        let phone =  txtPhoneNumber.text!
        uploadFile(image: profileImage){(isSuccess,message,imageURL) in
            var imageString = String()
            if isSuccess == true{
                imageString = imageURL!
            }
            else{
                displayAlert(title: "ErrorKey".localized, message: message!, viewcontroller: self)
                hideIndicator(view: self.view)
                return
            }
            userRegister(username: username, password:password, name: name, email: email, phone: phone, avatar: imageString){(success,message) in
                hideIndicator(view: self.view)
                FileManager.default.clearTmpDirectory()
                let end = NSDate()
                print("----------------------------------------------------")
                print("Response time:\(end.timeIntervalSince(start as Date))")
                print("----------------------------------------------------")
                if success == true {
                    displaySuccess(title: "SuccessKey".localized, message: "SignUpSucessfullKey".localized, viewcontroller: self)
                }
                else if success == false{
                    resetUserDefault()
                    displayAlert(title: "ErrorKey".localized, message: message!, viewcontroller: self)
                }
            }
        }
    }
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    func validate()->(pass:Bool,message:String?){
        if txtUsername.text == "" {
            return(false,"EmptyUsernameKey".localized)
        }
        if txtUsername.text!.count < 6{
            return(false,"ShortUsernameKey".localized)
        }
        if txtPassword.text!.count < 6{
            return (false,"ShortPasswordKey".localized)
        }
        if txtPassword.text! != txtRePassword.text!{
            return(false,"NotMatchPasswordKey".localized)
        }
        if txtName.text == ""{
            return (false,"EmptyNameKey".localized)
        }
        if txtName.text!.count < 4{
            return (false,"ShortNameKey".localized)
        }
        if txtPhoneNumber.text == ""{
            return (false,"EmptyPhoneKey".localized)
        }
        if txtPhoneNumber.text!.count < 10{
            return (false,"ShortPhoneKey".localized)
        }
        if txtEmail.text == ""{
            return (false,"EmptyEmailKey".localized)
        }
        if validateEmail(emailString: txtEmail.text!) == false {
            return (false,"WrongFormatEmailKey".localized)
        }
        return (true,nil)
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        tappedImage.previewImage(viewController: self)
        // Your action
    }


    
}
    
                   


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


