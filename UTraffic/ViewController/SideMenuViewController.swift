//
//  SideMenuViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 9/29/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import FacebookLogin
import Alamofire
import SwiftyJSON

class SideMenuViewController: UIViewController{

    @IBOutlet weak var txtUsername: UILabel!
    @IBOutlet weak var txtEmail: UILabel!
    @IBOutlet weak var btnSignOut: UIButton!
    @IBOutlet weak var btnLogIn: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var profileImage: UIRoundImage!
    @IBOutlet weak var contentMenu: UIStackView!
    @IBOutlet weak var userProfile: UIStackView!
    @IBOutlet weak var txtProfile: UILabel!
    @IBOutlet weak var menuView: UIRoundView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let profileTap = UITapGestureRecognizer(target: self, action: #selector(showProfile))
        userProfile.isUserInteractionEnabled = true
        userProfile.addGestureRecognizer(profileTap)
    }

    @objc func showProfile(){
        let PVC = parent as! MapViewController
        let storyboard = UIStoryboard(name: "UserProfile", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "profileVC") as! UserProfileViewController
        vc.curLat =  PVC.currentLat
        vc.curLong = PVC.currentLong
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }

    fileprivate func isLoggedIn() -> Bool {
        return UserDefaults.standard.bool(forKey: "isLoggedIn")
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        print("SideMenu")
        reloadPage()
        //loadUser(token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTAxfQ.L7EQt3XCAhnxHjX8XBArHNp-CGJxUOfrllkdUf1F3u4")

    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }

    @IBAction func SignOut(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "LogOutAskKey".localized, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "No".localized, style: UIAlertAction.Style.default, handler: {action in
        }))
        alert.addAction(UIAlertAction(title: "Yes".localized, style: UIAlertAction.Style.default, handler: {action in
            resetUserDefault()
            GIDSignIn.sharedInstance()?.signOut()
            let manager = LoginManager()
            manager.logOut()
            AccessToken.current = nil
            Profile.current = nil
            self.reloadPage()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    private func reloadPage(){
        print("Reloaded")
        if isLoggedIn() == true {
            txtUsername.isHidden = false
            txtEmail.isHidden = false
            btnLogIn.isHidden = true
            btnSignOut.isHidden = false
            btnSignUp.isHidden = true
            contentMenu.isHidden = false
            menuView.isHidden = false
            txtUsername.text =  UserDefaults.standard.string(forKey: "name")
            txtEmail.text = UserDefaults.standard.string(forKey: "email")
            if let imageURL = UserDefaults.standard.string(forKey: "avatarURL") {
                profileImage.load(url: URL(string: imageURL)!)
                profileImage.clipsToBounds = true
            }
            txtUsername.textColor = UIColor.black
        
            //profileImage.load(url: ))
        }else{
            txtUsername.isHidden = true
            txtEmail.isHidden = true
            btnLogIn.isHidden = false
            btnSignOut.isHidden = true
            btnSignUp.isHidden = false
            profileImage.image = UIImage(named: "person.crop.circle.fill")
            contentMenu.isHidden = true
            menuView.isHidden = true
            print("isLoggedIn() == false")
        }
        btnLogIn.setTitle("LogInKey".localized, for: .normal)
        btnSignUp.setTitle("SignUpKey".localized, for: .normal)
        btnSignOut.setTitle("LogOutKey".localized, for: .normal)
        txtProfile.text = "UserProfileKey".localized
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
