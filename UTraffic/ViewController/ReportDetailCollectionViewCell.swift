//
//  ReportDetailCollectionViewCell.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 11/25/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit

class ReportDetailCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var txtVelocity: UILabel!
    @IBOutlet weak var velocityLabel: UILabel!
    @IBOutlet weak var causeLabel: UILabel!
    @IBOutlet weak var txtCauses: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var avatar: UIRoundImage!
    @IBOutlet weak var image1: UIRoundImage!
    @IBOutlet weak var image2: UIRoundImage!
    @IBOutlet weak var image3: UIRoundImage!
    
    var userID = String()
    
}
