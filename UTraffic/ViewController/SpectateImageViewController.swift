//
//  SpectateImageViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 12/11/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit

class SpectateImageViewController: UIViewController {
    @IBOutlet weak var spectateImage: UIRoundImage!
    
    @IBOutlet weak var btnCancel: UIButton!
    var image = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        spectateImage.image = image
        spectateImage.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        btnCancel.setTitle("CancelKey".localized, for: .normal)
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
