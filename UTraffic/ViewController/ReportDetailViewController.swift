//
//  ReportDetailViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 11/22/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit

class ReportDetailViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    var ids = [String]()
    var fullname = [String]()
    var avatar =  [String]()
    var causes = [[String]]()
    var descriptions = [String]()
    var velocity = [String]()
    var images = [[String]]()
    var userID = [String]()
    var isValidated = [Bool]()
    let stockImage = UIImage(named: "person.crop.circle.fill")
    
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var reportCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ratingView.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        let dispatchGroup = DispatchGroup()
        let start2 = NSDate()
        var time = Double(0)
        var count = 0
        for id in self.ids{
            count = count + 1
            let start1 = NSDate()
            dispatchGroup.enter()
            getDetailTrafficReport(id: id){[weak self](isSuccess,message,name,ava,userId,vel,cause,des,imageURLs) in
                let end1 = NSDate()
                time = time + end1.timeIntervalSince(start1 as Date)
                if isSuccess{
                    if name != nil{
                        self!.fullname.append(name!)
                    }
                    else{
                        self!.fullname.append("")
                    }
                    if ava != nil{
                        self!.avatar.append(ava!)
                    }
                    else{
                        self!.avatar.append("")
                    }
                    if userId != nil{
                        self!.userID.append(userId!)
                    }
                    else{
                        self!.userID.append("")
                    }
                    if vel != nil {
                        self!.velocity.append(vel!)
                    }
                    else{
                        self!.velocity.append("")
                    }
                    if !cause!.isEmpty{
                        self!.causes.append(cause!)
                    }
                    else{
                        self!.causes.append([])
                    }
                    if !des!.isEmpty{
                        self!.descriptions.append(des!)
                    }
                    else{
                        self!.descriptions.append("")
                    }
                    if !imageURLs!.isEmpty{
                        self!.images.append(imageURLs!)
                    }
                    else{
                        self!.images.append([])
                    }
                    
                }
                else{
                    
                }
                dispatchGroup.leave()
            }
        }
        dispatchGroup.notify(queue: DispatchQueue.main){
            
            self.reportCollectionView.delegate = self
            self.reportCollectionView.dataSource = self
            self.reportCollectionView.reloadData()
            
            let end2 = NSDate()
            print("----------------------------------------------------")
            print("Number of report:",count)
            print("Request time:", time)
            print("Total time:",end2.timeIntervalSince(start2 as Date) + time)
            print("----------------------------------------------------")
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ids.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reportId", for: indexPath) as! ReportDetailCollectionViewCell
        cell.txtName.text = fullname[indexPath.row]
        let avatarURL = avatar[indexPath.row]
        if avatarURL == ""{
            cell.avatar.image = self.stockImage
            cell.avatar.tintColor = .gray
        }
        else{
            cell.avatar.load(url: URL(string: avatarURL)!)
        }
        cell.avatar.clipsToBounds = true
        
        let uID = userID[indexPath.row]
        if uID != "" {
            cell.userID = uID
        }
        else{
            cell.userID = ""
        }
        
        let vel = velocity[indexPath.row]
        if vel != ""{
            cell.txtVelocity.text = vel
        }else{
            cell.txtVelocity.text = "-"
        }
        
        let c = causes[indexPath.row]
        if !c.isEmpty{
            cell.txtCauses.text = c.joined(separator: ",")
        }else{
            cell.txtCauses.text = "-"
        }
        let d = descriptions[indexPath.row]
        if d != ""{
            cell.txtDescription.text = d
        }
        else{
            cell.txtDescription.text = "NoDescriptionKey".localized
        }
        
        var imageList = [UIImageView]()
        imageList.append(cell.image1)
        imageList.append(cell.image2)
        imageList.append(cell.image3)
        let urls = images[indexPath.row]

        for (i,image) in imageList.enumerated(){
            if i <= (urls.count-1) && urls.count != 0 {
                image.isHidden = false
                image.load(url: URL(string: urls[i])!)
                image.clipsToBounds = true
            }
            else{
                image.isHidden = true
            }
        }
        let longPressGestureRecognizer1 = UILongPressGestureRecognizer(target: self, action: #selector(imageLongPressed))
        let longPressGestureRecognizer2 = UILongPressGestureRecognizer(target: self, action: #selector(imageLongPressed))
        let longPressGestureRecognizer3 = UILongPressGestureRecognizer(target: self, action: #selector(imageLongPressed))
        cell.image1.isUserInteractionEnabled = true
        cell.image2.isUserInteractionEnabled = true
        cell.image3.isUserInteractionEnabled = true
        cell.image1.addGestureRecognizer(longPressGestureRecognizer1)
        cell.image2.addGestureRecognizer(longPressGestureRecognizer2)
        cell.image3.addGestureRecognizer(longPressGestureRecognizer3)
        
        isValidated.append(false)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if UserDefaults.standard.bool(forKey: "isLoggedIn") == true {
            if userID[indexPath.row] != UserDefaults.standard.string(forKey: "userID") {
                if isValidated[indexPath.row] == false{
                    let CVC = children[0] as! RatingViewController
                    CVC.reportId = ids[indexPath.row]
                    CVC.index = indexPath.row
                    CVC.starsView.value = 0
                    setView(view: self.ratingView, isHidden: false)
                }
                else{
                    displayAlert(title: "ErrorKey".localized, message: "NoReEvaluateKey".localized, viewcontroller: self)
                }
            }
            else{
                displayAlert(title: "ErrorKey".localized, message: "NoSelfEvaluateKey".localized, viewcontroller: self)
            }
        }
        else {
            displayAlert(title: "ErrorKey".localized, message: "AskToLogInKey".localized, viewcontroller: self)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 380 , height: 300)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        CGSize()
    }
    func setView(view:UIView,isHidden:Bool){
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = isHidden
        })
    }
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    func displaySuccessEvaluate(){
        displaySuccess(title: "SuccessKey".localized, message: "SuccessEvaluateKey".localized, viewcontroller: self)
    }
    @objc func imageLongPressed(_ sender: UIGestureRecognizer)
    {
        if sender.state == .ended{
            let tappedImage = sender.view as! UIImageView
            tappedImage.previewImage(viewController: self)
        }else if sender.state == .began{
            print("Long press")
        }
    }
    


}
