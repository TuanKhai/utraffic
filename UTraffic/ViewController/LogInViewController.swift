//
//  LogInViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 9/11/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit
import TextFieldEffects
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

class LogInViewController: UIViewController,GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
          if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
            print("The user has not signed in before or they have since signed out.")
          } else {
            print("\(error.localizedDescription)")
          }
          return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        googleSignIn(userId: userId!, token: idToken!){[weak self](isSuccess,message) in
            if isSuccess == true{
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
                self!.afterLogIn(accessToken: accessToken!)
            }
            else {
                displayAlert(title: "ErrorKey".localized, message: "FacebookErrorKey".localized, viewcontroller: self!)
            }
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        UserDefaults.standard.removeObject(forKey: "username")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "profileImageUrl")
        UserDefaults.standard.synchronize()
      // Perform any operations when the user disconnects from app here.
      // ...
        print(UserDefaults.standard.bool(forKey: "isLoggedIn"))
        print("disconnected")
    }


    
    

    @IBOutlet weak var txtPassword: HoshiTextField!
    @IBOutlet weak var txtUsername: HoshiTextField! 
    @IBOutlet var holderView: UIView!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var btnLogIn: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnFacebookLogIn: FBLoginButton!
    @IBOutlet weak var btnGoogleSignIn: GIDSignInButton!
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        holderView.addGestureRecognizer(tapGesture)
        tapGesture.cancelsTouchesInView = false
        // Do any additional setup after loading the view.
        

        
    }
    @IBAction func forgotPassword(_ sender: Any) {
        displayAlert(title: nil, message: "ComingSoonKey".localized, viewcontroller: self)
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        holderView.endEditing(true)
    }
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func googleLogIn(_ sender: Any) {
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func facebookLogIn(_ sender: Any) {
        loginButtonClicked()
    }
    func loginButtonClicked() {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [ .publicProfile,.email ], viewController: self) { loginResult in
            print(loginResult)
            let r = GraphRequest(graphPath: "me", parameters: ["fields":"email,id,name,picture.type(large)"], tokenString: AccessToken.current?.tokenString, version: nil, httpMethod: .get)
            r.start(completionHandler: {[weak self](test, result, error) in
                if(error == nil)
                {
                    let json = JSON(result!)
                    let id = json["id"].string!
                    let accessToken = AccessToken.current!.tokenString
                    facebookSignIn(userId: id, token: accessToken){(isSuccess,message) in
                        if isSuccess == true{
                    let accessToken = UserDefaults.standard.string(forKey: "accessToken")
                            self!.afterLogIn(accessToken: accessToken!)
                        }
                        else {
                            displayAlert(title: "ErrorKey".localized, message: "FacebookErrorKey".localized, viewcontroller: self!)
                        }
                    }
                }
                else{
                    print(error!)
                }
            })
        }
    }
    @IBAction func LogIn(_ sender: Any) {
        print("button pressed")
        let start1 = NSDate()
        if NetworkConnection.isConnectedToNetwork() == false {
            displayAlert(title: "ErrorKey".localized, message: "NetworkKey".localized, viewcontroller: self)
            return
        }
        let username = txtUsername.text
        let password = txtPassword.text
        let validation = validate()
        if validation.pass == true {
            let start2 = NSDate()
            showIndicator(text: "LoadingKey".localized, view: self.view)
            normalLogIn(username: username!, password: password!){[weak self](isSuccess,message) in
                let end2 = NSDate()
                hideIndicator(view: self!.view)
                print("Response time: \(end2.timeIntervalSince(start2 as Date))")
                if isSuccess == true{
                    let accessToken = UserDefaults.standard.string(forKey: "accessToken")
                    self!.afterLogIn(accessToken: accessToken!)
                }
                else if isSuccess == false{
                    displayAlert(title: "ErrorKey".localized, message: message!,viewcontroller: self!)
                }
                let end1 = NSDate()
                print("Process time: \(end1.timeIntervalSince(start1 as Date))")
            }
        }
        else {
            displayAlert(title: "ErrorKey".localized, message: validation.message!, viewcontroller: self)
        }
    }

    func afterLogIn(accessToken:String){
        showIndicator(text: "LoadingKey".localized, view: self.view)
        getCurrentUserInfo(accessToken: accessToken){[weak self](isSuccess,message) in
            hideIndicator(view: self!.view)
            if isSuccess == true {
                displaySuccess(title: "SuccessKey".localized, message: "SuccessLoginKey".localized, viewcontroller: self!)
            }
            else if isSuccess == false{
                resetUserDefault()
                displayAlert(title: "ErrorKey".localized, message: message!, viewcontroller: self!)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        print("Logged In")
        txtUsername.placeholder = "UsernameKey".localized
        txtPassword.placeholder = "PasswordKey".localized
        orLabel.text = "OrKey".localized
        btnLogIn.setTitle("LogInKey".localized, for: .normal)
        btnSignUp.setTitle("SignUpKey".localized, for: .normal)
        btnForgotPassword.setTitle("ForgotPasswordKey".localized, for: .normal)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }

    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        resetUserDefault()
    }
    func loginButtonWillLogin(_ loginButton: FBLoginButton) -> Bool {
        return true
    }
    func validate()->(pass:Bool,message:String?){
        if txtUsername.text == "" && txtPassword.text == ""{
            return (false, "CheckUsernamePasswordKey".localized)
        }
        if txtUsername.text == "" {
            return (false, "CheckUsernameKey".localized)
        }
        if txtPassword.text == "" {
            return (false, "CheckPasswordKey".localized)
        }
        if txtUsername.text!.count < 6{
            return(false,"ShortUsernameKey".localized)
        }
        if txtPassword.text!.count < 6{
            return (false,"ShortPasswordKey".localized)
        }
        return (true,nil)
    }

}

