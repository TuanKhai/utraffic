//
//  SearchLocationViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 11/7/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import SwiftyJSON

class SearchLocationViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtSearchPlaces: DesignableUITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearchPlaces.delegate = self
        // Do any additional setup after loading the view.
    }
    @IBAction func searchTextFieldTapped(_ sender: Any) {
        txtSearchPlaces.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        acController.delegate = self as GMSAutocompleteViewControllerDelegate
        present(acController, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        txtSearchPlaces.placeholder = "SearchPlacesKey".localized
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        textField.resignFirstResponder()
        let PVC = parent as! MapViewController
        PVC.resetMarkers()
        return false;
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SearchLocationViewController: GMSAutocompleteViewControllerDelegate {
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    // Get the place name from 'GMSAutocompleteViewController'
    // Then display the name in textField
    let PVC = parent as! MapViewController
    PVC.placeMarker1.icon = GMSMarker.markerImage(with: .red)
    PVC.mapView.animate(toLocation: place.coordinate)
    PVC.mapView.animate(toZoom: 16)
    PVC.placeMarker1.map = PVC.mapView
    txtSearchPlaces.text = place.name
//    PVC.setMarker1(Lat: place.coordinate.latitude, Long: place.coordinate.longitude, Title: nil, Snippet: nil, imageString: nil, map: PVC.mapView)
    PVC.placeMarker1.position = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
    PVC.getNearTrafficStatus(searchlat: place.coordinate.latitude, searchlong: place.coordinate.longitude)
    dismiss(animated: true, completion: nil)
  }
func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // Handle the error
    print("Error: ", error.localizedDescription)
  }
func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    // Dismiss when the user canceled the action
    dismiss(animated: true, completion: nil)
  }
}
