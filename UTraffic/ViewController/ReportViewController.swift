//
//  ReportViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 9/18/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//
import Alamofire
import SwiftyJSON
import UIKit
import TextFieldEffects
import MBProgressHUD
import DCKit
import CoreLocation
class ReportViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,UITextFieldDelegate {


    @IBOutlet weak var reportLocationSegmentsControl: UISegmentedControl!
    @IBOutlet weak var txtSpeed: DesignableUITextField!
    @IBOutlet weak var txtBefore: DesignableUITextField!
    @IBOutlet weak var txtAfter: DesignableUITextField!
    @IBOutlet weak var txtMessurement: UITextField!
    @IBOutlet weak var txtReason: DesignableUITextField!
    @IBOutlet weak var btnSpeech: RoundButton!
    @IBOutlet weak var txtReport: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnClearImage1: RoundButton!
    @IBOutlet weak var btnClearImage2: RoundButton!
    @IBOutlet weak var btnClearImage3: RoundButton!
    
    @IBOutlet weak var txtDescription: UICustomTextView!
    @IBOutlet weak var btnGetDirection: UIButton!
    @IBOutlet weak var trafficImage1: UIImageView!
    @IBOutlet weak var trafficImage2: UIImageView!
    @IBOutlet weak var trafficImage3: UIImageView!
    
    var messurement = ["km/h","miles/h"]
    var reason = ["TrafficJamKey".localized,"RoadFloodKey".localized,"BlockageKey".localized,"AccidentKey".localized,"PoliceKey".localized,"ClosedRoadKey".localized,"OtherKey".localized]
    var curLat = Double()
    var curLong = Double()
    var refLat = Double()
    var refLong = Double()
    var imageTag = Int()
    var reportType = String()
    var trafficImages:[UIImageView] = []
    var clearButtons:[RoundButton] = []
    var reasonId = Int()
    var userSpeed :Int = 0
    var imagePath = ["","",""]

    override func viewDidLoad() {
        super.viewDidLoad()
        txtSpeed.text = String(userSpeed)
        // Do any additional setup after loading the view.
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        txtReason.inputView = pickerView
        txtMessurement.inputView = pickerView
      
        disableTrafficImage2()
        disableTrafficImage3()
        disableClearButton1()
        disableClearButton2()
        disableClearButton3()
        
        trafficImages = [trafficImage1,trafficImage2,trafficImage3]
        clearButtons = [btnClearImage1,btnClearImage2,btnClearImage3]
        addTag(images: trafficImages)
        addTag(buttons: clearButtons)
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        tapGestureRecognizer1.numberOfTouchesRequired = 1
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        tapGestureRecognizer2.numberOfTouchesRequired = 1
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        tapGestureRecognizer3.numberOfTouchesRequired = 1
        let longPressGestureRecognizer1 = UILongPressGestureRecognizer(target: self, action: #selector(imageLongPressed))
        let longPressGestureRecognizer2 = UILongPressGestureRecognizer(target: self, action: #selector(imageLongPressed))
        let longPressGestureRecognizer3 = UILongPressGestureRecognizer(target: self, action: #selector(imageLongPressed))
        trafficImage1.isUserInteractionEnabled = true
        trafficImage1.addGestureRecognizer(tapGestureRecognizer1)
        trafficImage1.addGestureRecognizer(longPressGestureRecognizer1)
        trafficImage2.isUserInteractionEnabled = true
        trafficImage2.addGestureRecognizer(tapGestureRecognizer2)
        trafficImage2.addGestureRecognizer(longPressGestureRecognizer2)
        trafficImage3.isUserInteractionEnabled = true
        trafficImage3.addGestureRecognizer(tapGestureRecognizer3)
        trafficImage3.addGestureRecognizer(longPressGestureRecognizer3)
        txtDescription.delegate = self
        txtSpeed.delegate = self
        txtBefore.delegate = self
        txtAfter.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        txtReport.text="ReportKey".localized
        reportLocationSegmentsControl.setTitle("CurrentLocationKey".localized, forSegmentAt: 0)
        reportLocationSegmentsControl.setTitle("DifferentLocation".localized, forSegmentAt: 1)
        btnGetDirection.setTitle("GetDirectionKey".localized, for: .normal)
        txtSpeed.placeholder = "SpeedKey".localized
        txtBefore.placeholder = "BehindYouKey".localized
        txtAfter.placeholder = "InFrontOfYouKey".localized
        txtReason.placeholder = "CauseKey".localized
        descriptionLabel.text = "DescriptionKey".localized
        imageLabel.text = "ImageKey".localized
        btnSend.setTitle("SendKey".localized, for: .normal)

    }
    override func viewWillDisappear(_ animated: Bool) {
         AppUtility.lockOrientation(.all)
    }
    @objc func imageLongPressed(_ sender: UIGestureRecognizer)
    {
        if sender.state == .ended{
            let tappedImage = sender.view as! UIImageView
            tappedImage.previewImage(viewController: self)
        }else if sender.state == .began{
            print("Long press")
        }
    }
    
    @objc func imageTapped(_ sender: UIGestureRecognizer)
    {
        print("Tab")
        imageTag = sender.view!.tag
        print("ImageTag:",imageTag)
           let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
           alert.addAction(UIAlertAction(title: "TakePictureKey".localized, style: .default , handler:{ (UIAlertAction)in
               print("Camera")
               self.getImage(fromSourceType: .camera)
           }))

           alert.addAction(UIAlertAction(title: "ChooseFromLibraryKey".localized, style: .default , handler:{ (UIAlertAction)in
               print("Library")
               self.getImage(fromSourceType: .photoLibrary)
           }))

        alert.addAction(UIAlertAction(title: "CancelKey".localized, style: .cancel, handler:{ (UIAlertAction)in
               print("User click Dismiss button")
           }))

           self.present(alert, animated: true, completion: {
               print("completion block")
           })

        // Your action
    }

    @IBAction func changeReportForm(_ sender: Any) {
        if (sender as! UISegmentedControl).selectedSegmentIndex == 0 {
            print("First Segment Select")
            btnGetDirection.setTitle("GetDirectionKey".localized, for: .normal)
            txtBefore.isHidden = false
            txtAfter.isHidden = false
        }
        else if (sender as! UISegmentedControl).selectedSegmentIndex == 1
        {
            print("Second Segment Select")
            btnGetDirection.setTitle("GetLocationKey".localized, for: .normal)
            txtBefore.isHidden = true
            txtAfter.isHidden = true
        }
    }
    @IBAction func getDirection(_ sender: Any) {
        let PVC = parent as! MapViewController
        if reportLocationSegmentsControl.selectedSegmentIndex == 0{
            PVC.mapView.clear()
            PVC.closeReportMenu()
            PVC.beginGetDirection = true
            PVC.beginGetLocation = false
            PVC.reCenter(zoom: 20)
            PVC.hideUI()
            PVC.currentDirection()
        }
        if reportLocationSegmentsControl.selectedSegmentIndex == 1{
            PVC.mapView.clear()
            PVC.closeReportMenu()
            PVC.beginGetDirection = false
            PVC.beginGetLocation = true
            PVC.hideUI()
            PVC.setDirectionMarker()
        }
    }
    
    @IBAction func send(_ sender: Any) {
        var start1 = NSDate()
        var start2 = NSDate()
        var end1 = NSDate()
        var end2 = NSDate()
        var vel = String()
        var causes = [String]()
        var des = String()
        let dispatchGroup = DispatchGroup()
        var startPoint = CLLocationCoordinate2D(latitude: curLat, longitude: curLong)
        var endPoint = CLLocationCoordinate2D(latitude: refLat, longitude: refLong)
        let PVC = parent as! MapViewController
        var err = false
        PVC.mapView.clear()
        //print("text:",reasonId,distance!,behind!,vel!)
        if validateTextFields() == false {
            return
        }
        if txtReason.text != ""{
            causes.append(txtReason.text!)
        }
        vel = txtSpeed.text!
        des = txtDescription.text

        if txtBefore.text != "" || txtAfter.text != "" {
            let sPoint:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: curLat, longitude: curLong)
            let ePoint:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: refLat, longitude: refLong)
            if txtBefore.text != "" {
                let before:Double  = Double(txtBefore.text!)!
                let sbearing = getBearingBetweenTwoPoints(point1: ePoint, point2: sPoint)
                startPoint = getFarLocation(location: sPoint, byMovingDistance:before , withBearing: sbearing)
                //PVC.setTestMarker(coordinate: startPoint, color: .red, map: PVC.mapView)
               
            }
            if txtAfter.text != ""{
                let after:Double = Double(txtAfter.text!)!
                let ebearing = getBearingBetweenTwoPoints(point1: sPoint, point2: ePoint)
                endPoint = getFarLocation(location: sPoint, byMovingDistance: after, withBearing: ebearing)
                //PVC.setTestMarker(coordinate: endPoint, color: .yellow, map: PVC.mapView)
                
            }
//            PVC.setTestMarker(coordinate: sPoint, color: .blue, map: PVC.mapView)
//            PVC.setTestMarker(coordinate: ePoint, color: .green, map: PVC.mapView)
//            PVC.drawPolyline(coordinate1: startPoint, coordinate2: endPoint, color: .purple, map: PVC.mapView)
//            PVC.drawPolyline(coordinate1: sPoint, coordinate2: ePoint, color: .red, map: PVC.mapView)
//            print("Start Point:",sPoint.latitude,sPoint.longitude)
//            print("End Point:",ePoint.latitude,ePoint.longitude)
//            print("Before Point:",startPoint.latitude,startPoint.longitude)
//            print("After Point:",endPoint.latitude,endPoint.longitude)
//            print("Distance Before:", CLLocation(latitude: sPoint.latitude, longitude: sPoint.longitude).distance(from: CLLocation(latitude: startPoint.latitude, longitude: startPoint.longitude)))
//            print("Distance After:",CLLocation(latitude: sPoint.latitude, longitude: sPoint.longitude).distance(from: CLLocation(latitude: endPoint.latitude, longitude: endPoint.longitude)))

        }
        showIndicator(text: "LoadingKey".localized, view: self.view)
        dispatchGroup.enter()
        snapToRoad(lat: startPoint.latitude, long: startPoint.longitude){[weak self](isSucess,lat,long) in
            if isSucess == true{
                startPoint.latitude = lat!
                startPoint.longitude = long!
                snapToRoad(lat: endPoint.latitude, long: endPoint.longitude){[weak self](isSucess,lat,long) in
                    if isSucess == true{
                        endPoint.latitude = lat!
                        endPoint.longitude = long!
                    }
                    else {
                        err = true
                    }
                    dispatchGroup.leave()
                }
            }
            else {
                err = true
                dispatchGroup.leave()
            }

        }
        dispatchGroup.notify(queue: .main){
            if err == false {
                var imageList = self.getImageListByClearButtons()
                start2 = NSDate()
                submitTrafficReportsRemote(accessToken: UserDefaults.standard.string(forKey: "accessToken")!, startLat: startPoint.latitude, startLng: startPoint.longitude, endLat: endPoint.latitude, endLng: endPoint.longitude, velocity: vel, description: des, causes: causes,images: imageList){[weak self](isSuccess,message)in
                    hideIndicator(view: self!.view)
                    FileManager.default.clearTmpDirectory()
                    end2 = NSDate()
                    if isSuccess == true {
                        imageList.removeAll()
                        self!.resetForms()
                        PVC.handleDismiss()
                        PVC.showSnackBar(message: "SuccessReportKey".localized)
                    }
                    else if isSuccess == false {
                        self!.displayAlert(title: "ErrorKey".localized, message: message!)
                    }
                    let end1 = NSDate()
                    print("----------------------------------------------------")
                    print("Start Location:\(startPoint)")
                    print("End location:\(endPoint)")
                    print("Response time:",end2.timeIntervalSince(start2 as Date))
                    print("Total time:",end1.timeIntervalSince(start1 as Date))
                    print("----------------------------------------------------")
                }
            }
            else {
                hideIndicator(view: self.view)
                self.displayAlert(title: "ErrorKey".localized, message: "ServerErrorKey".localized)
            }
        }
    }
    func resetForms(){
        clearImage3(self)
        clearImage2(self)
        clearImage1(self)
        disableTrafficImage2()
        disableTrafficImage3()
        disableClearButton1()
        disableClearButton2()
        disableClearButton3()
        txtDescription.text = nil
        txtReason.text = nil
        txtSpeed.text = nil
        txtBefore.text = nil
        txtAfter.text = nil
        
    }
    func getImageListByClearButtons()-> [UIImageView]{
        var list = [UIImageView]()
        for button in clearButtons{
            if button.alpha != 0 {
                list.append(trafficImages[button.tag])
            }
        }
        return list
    }
    func validateTextFields()->(Bool){
        if txtSpeed.text == ""{
            displayAlert(title: "ErrorKey".localized, message: "EmptySpeedKey".localized)
            return false
        }
        if curLat == 0 || curLong == 0 || refLat == 0 || refLong == 0{
            displayAlert(title: "ErrorKey".localized, message: "EmptyLocationKey".localized)
            return false
        }
        if UserDefaults.standard.string(forKey: "accessToken") == nil{
            displayAlert(title: "ErrorKey".localized, message: "AskToLogInKey".localized)
            return false
        }
        if txtReason.text == "" {
            displayAlert(title: "ErrorKey".localized, message: "EmptyReasonKey".localized)
            return false
        }
        return true
    }
    func degreesToRadians(degrees: Double) -> Double {
        return Measurement(value: degrees, unit: UnitAngle.degrees).converted(to: .radians).value
    }

    func radiansToDegrees(radians: Double) -> Double {
        return Measurement(value: radians, unit: UnitAngle.radians).converted(to: .degrees).value
    }

    func getFarLocation(location: CLLocationCoordinate2D, byMovingDistance distance: Double, withBearing bearingDegrees:Double) -> CLLocationCoordinate2D {
        let distanceRadians: Double = distance / 6371000
        let bearingRadians: Double = bearingDegrees

        let lat1 = degreesToRadians(degrees: location.latitude)
        let lon1 = degreesToRadians(degrees: location.longitude)

        let lat2 = radiansToDegrees(radians:asin(sin(lat1) * cos(distanceRadians) + cos(lat1) * sin(distanceRadians) * cos(bearingRadians)))
        let lon2 = radiansToDegrees(radians:lon1 + atan2(sin(bearingRadians) * sin(distanceRadians * cos(lat1)), cos(distanceRadians) - sin(lat1) * sin(lat2)))

        return CLLocationCoordinate2D(latitude: lat2, longitude: lon2)
    }

    func getBearingBetweenTwoPoints(point1 : CLLocationCoordinate2D, point2 : CLLocationCoordinate2D) -> Double {

        let lat1 = degreesToRadians(degrees: point1.latitude)
        let lon1 = degreesToRadians(degrees: point1.longitude)

        let lat2 = degreesToRadians(degrees: point2.latitude)
        let lon2 = degreesToRadians(degrees: point2.longitude)

        let dLon = lon2 - lon1

        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)

        return radiansBearing
    }
    
    // MARK - Populate pickerview
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var value:Int = 0
        if txtMessurement.isFirstResponder {
            value = messurement.count
        }else if txtReason.isFirstResponder
        {
            value = reason.count
        }
       return value
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if txtMessurement.isFirstResponder {
            txtMessurement.text = messurement[row]
        }else if txtReason.isFirstResponder
        {
            txtReason.text = reason[row]
            reasonId = row+1
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if txtMessurement.isFirstResponder {
            return messurement[row]
        }else if txtReason.isFirstResponder
        {
            return reason[row]
        }
        return nil
    }
    
    func changeCurrentSpeed(speed:String){
        txtSpeed.text = speed
    }
    
    func setValues(curLat:Double,curLong:Double,refLat:Double,refLong:Double){
        self.curLat = curLat
        self.curLong = curLong
        self.refLat = refLat
        self.refLong = refLong
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //var selectedImage: UIImage?
        let date = Date()
        let interval = ((date.timeIntervalSince1970)*1000).rounded()
        let imageName = String(Int(interval))
        for trafficImage in trafficImages{
            if trafficImage.tag == imageTag {
                if let editedImage = info[.editedImage] as? UIImage {
                    imagePath[imageTag] = saveImageToTempDirectory(image: editedImage, withName: imageName)!
                    trafficImage.image = loadImageFromTempDirectory(imageName: imageName)
                    trafficImage.alpha = 1
//                    selectedImage = editedImage
//                    trafficImage.image = selectedImage!
//                    trafficImage.alpha = 1
                    trafficImages.append(trafficImage)
                    picker.dismiss(animated: true, completion: nil)
                } else if let originalImage = info[.originalImage] as? UIImage {
                    imagePath[imageTag] = saveImageToTempDirectory(image: originalImage, withName: imageName)!
                    trafficImage.image = loadImageFromTempDirectory(imageName: imageName)
//                    selectedImage = originalImage
//                    trafficImage.image = selectedImage!
//                    trafficImage.alpha = 1
                    trafficImages.append(trafficImage)
                    picker.dismiss(animated: true, completion: nil)
                }
                for button in clearButtons {
                    if button.tag == imageTag
                    {
                        button.alpha = 1
                        button.isUserInteractionEnabled = true
                    }
                }
            }
            if trafficImage.tag == (imageTag+1){
                trafficImage.alpha = 0.5
                trafficImage.isUserInteractionEnabled = true
            }
        }
    }
    func addTag(images: [UIImageView]){
        var i = 0;
        for image in images{
            image.tag = i
            i += 1
        }
    }
    func addTag(buttons: [RoundButton]){
        var i = 0;
        for button in buttons{
            button.tag = i
            i += 1
        }
    }
    @IBAction func clearImage1(_ sender: Any) {
        if btnClearImage2.alpha == 0 {
            trafficImage1.image = UIImage(named: "photo.on.rectangle")
            trafficImage1.alpha = 0.5
            disableClearButton1()
            disableTrafficImage2()
        }
        else if btnClearImage2.alpha == 1 {
            if btnClearImage3.alpha == 1{
                trafficImage1.image = trafficImage2.image
                trafficImage2.image = trafficImage3.image
                trafficImage3.image = UIImage(named: "photo.on.rectangle")
                trafficImage3.alpha = 0.5
                disableClearButton3()
            }
            else{
                trafficImage1.image = trafficImage2.image
                trafficImage2.image = UIImage(named: "photo.on.rectangle")
                trafficImage2.alpha = 0.5
                disableClearButton2()
                disableTrafficImage3()
            }
        }
        
    }
    @IBAction func clearImage2(_ sender: Any) {
        if btnClearImage3.alpha == 0{
            trafficImage2.image = UIImage(named: "photo.on.rectangle")
            trafficImage2.alpha = 0.5
            disableClearButton2()
            disableTrafficImage3()
        }
        else if btnClearImage3.alpha == 1{
            trafficImage2.image = trafficImage3.image
            trafficImage3.image = UIImage(named: "photo.on.rectangle")
            trafficImage3.alpha = 0.5
            disableClearButton3()
        }
    }
    @IBAction func clearImage3(_ sender: Any) {
        trafficImage3.image = UIImage(named: "photo.on.rectangle")
        trafficImage3.alpha = 0.5
        disableClearButton3()
    }
    func disableTrafficImage2(){
        trafficImage2.alpha = 0
        trafficImage2.isUserInteractionEnabled = false
    }
    func disableTrafficImage3(){
        trafficImage3.alpha = 0
        trafficImage3.isUserInteractionEnabled = false
    }
    func disableClearButton1(){
        btnClearImage1.alpha = 0
        btnClearImage1.isUserInteractionEnabled = false
    }
    func disableClearButton2(){
        btnClearImage2.alpha = 0
        btnClearImage2.isUserInteractionEnabled = false
    }
    func disableClearButton3(){
        btnClearImage3.alpha = 0
        btnClearImage3.isUserInteractionEnabled = false
    }
    
    func enableTrafficImage2(){
        trafficImage2.alpha = 0.5
        trafficImage2.isUserInteractionEnabled = true
    }
    func enableTrafficImage3(){
        trafficImage3.alpha = 0.5
        trafficImage3.isUserInteractionEnabled = true
    }
    func enableClearButton1(){
        btnClearImage1.alpha = 1
        btnClearImage1.isUserInteractionEnabled = true
    }
    func enableClearButton2(){
        btnClearImage2.alpha = 1
        btnClearImage2.isUserInteractionEnabled = true
    }
    func enableClearButton3(){
        btnClearImage3.alpha = 1
        btnClearImage3.isUserInteractionEnabled = true
    }

    func displayAlert(title:String,message:String){
        // create the alert
         let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
         // add an action (button)
         alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
         // show the alert
         self.present(alert, animated: true, completion: nil)
    }
//    @IBAction func enteringInFrontValue(_ sender: Any) {
//        checkMaxLength(textField: txtAfter, maxLength: 5)
//    }
//    @IBAction func enteringBehindValue(_ sender: Any) {
//        checkMaxLength(textField: txtAfter, maxLength: 5)
//    }
    func textViewDidChange(_ textView: UITextView) {
        let badwords = appDelegate.profanityList_EN + appDelegate.profanityList_VN
        var badwordString = badwords.joined(separator: "|")
        badwordString = "\\b(\(badwordString))\\b"
        print(badwordString)
        let res = textView.text
        textView.text = ProfanityFilter.sharedInstance.cleanUp(forString: badwordString, inString: res!)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
             let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                 return false
         }
         let substringToReplace = textFieldText[rangeOfTextToReplace]
         let count = textFieldText.count - substringToReplace.count + string.count
         return count <= 5
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars <= 200
    }

    
    
 
}


    



