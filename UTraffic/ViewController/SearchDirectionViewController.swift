//
//  SearchDirectionViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 10/16/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import Alamofire
import SwiftyJSON

class SearchDirectionViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet var holderView: UIView!
    @IBOutlet weak var txtFrom: UISearchTextField!
    @IBOutlet weak var txtTo: UISearchTextField!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var routeSelectSegmentControl: UISegmentedControl!
    var textfieldTapped = Int()
    var fLat = Double()
    var fLong = Double()
    var tLat = Double()
    var tLong = Double()
    var isSearching = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        isSearching = false
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        holderView.addGestureRecognizer(tapGesture)
        tapGesture.cancelsTouchesInView = false
        // Do any additional setup after loading the view.
        txtTo.delegate = self
        txtFrom.delegate = self
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        holderView.endEditing(true)
    }
    @IBAction func searchDirection(_ sender: Any) {
        if validateFields() == true{
            switch routeSelectSegmentControl.selectedSegmentIndex {
            case 0:
                findShortestRoute()
                isSearching = true

            case 1:
                findFastesRoute()
                isSearching = true
            default:
                break
            }
        }
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.text = ""
        textField.resignFirstResponder()
        let PVC = parent as! MapViewController
        PVC.resetMarkers()
        PVC.routePolyline1.map = nil
        return false;
    }
    func findShortestRoute(){
        let start1 = NSDate()
        let PVC = parent as! MapViewController
        PVC.resetMarkers()
        showIndicator(text: "LoadingKey".localized, view: PVC.view)
        let start2 = NSDate()
        findRoute(flat: fLat, flong: fLong, tlat: tLat, tlong: tLong, type: "distance"){
            (isSuccess,message,coords1,coords2) in
            let end2 = NSDate()
            print("Request Time:",end2.timeIntervalSince(start2 as Date))
            hideIndicator(view: PVC.view)
            if isSuccess == false {
                displayAlert(title: "ErrorKey".localized, message: message!, viewcontroller: self)
                return
            }
            PVC.placeMarker1.map = PVC.mapView
            PVC.placeMarker2.map = PVC.mapView
            PVC.placeMarker1.position = CLLocationCoordinate2D(latitude: self.fLat, longitude: self.fLong)
            PVC.placeMarker2.position = CLLocationCoordinate2D(latitude: self.tLat, longitude: self.tLong)
            PVC.drawDirection1(coordinateList: coords1!, color: UIColor.systemGreen, map: PVC.mapView)
            let end1 = NSDate()
            print("Finished Time: ",end1.timeIntervalSince(start1 as Date))
        }
    }
    func findFastesRoute(){
        let start1 = NSDate()
        let PVC = parent as! MapViewController
        PVC.resetMarkers()
        showIndicator(text: "LoadingKey".localized, view: PVC.view)
        let start2 = NSDate()
        findRoute(flat: fLat, flong: fLong, tlat: tLat, tlong: tLong, type: "time"){
            (isSuccess,message,coords1,coords2) in
            let end2 = NSDate()
            print("Request Time:",end2.timeIntervalSince(start2 as Date))
            hideIndicator(view: PVC.view)
            if isSuccess == false {
                displayAlert(title: "ErrorKey".localized, message: message!, viewcontroller: self)
                return
            }
            PVC.placeMarker1.map = PVC.mapView
            PVC.placeMarker2.map = PVC.mapView
            PVC.placeMarker1.position = CLLocationCoordinate2D(latitude: self.fLat, longitude: self.fLong)
            PVC.placeMarker2.position = CLLocationCoordinate2D(latitude: self.tLat, longitude: self.tLong)
            PVC.drawDirection1(coordinateList: coords1!, color: UIColor.systemGreen, map: PVC.mapView)
            let end1 = NSDate()
            print("Finished Time: ",end1.timeIntervalSince(start1 as Date))
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        fromLabel.text = "FromKey".localized
        toLabel.text = "ToKey".localized
        btnSearch.setTitle("SearchKey".localized, for:.normal )
        routeSelectSegmentControl.setTitle("ShortestRouteKey".localized, forSegmentAt: 0)
        routeSelectSegmentControl.setTitle("FastestRouteKey".localized, forSegmentAt: 1)
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    @IBAction func fromTextFieldTapped(_ sender: Any) {
        textfieldTapped = 0
        txtFrom.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        acController.delegate = self as GMSAutocompleteViewControllerDelegate
        present(acController, animated: true, completion: nil)
    }
    @IBAction func toTextFieldTapped(_ sender: Any) {
        textfieldTapped = 1
        txtTo.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        acController.delegate = self as GMSAutocompleteViewControllerDelegate
        present(acController, animated: true, completion: nil)
    }
    func validateFields() -> Bool{
        if txtFrom.text == "" {
            displayAlert(title: "ErrorKey".localized, message: "NoFromKey".localized, viewcontroller: self)
            return false
        }
        else if txtTo.text == "" {
            displayAlert(title: "ErrorKey".localized, message: "NoToKey".localized, viewcontroller: self)
            return false
        }
        return true
    }
    func setMarker(coordinate:CLLocationCoordinate2D){
        let PVC = parent as? MapViewController
//        let marker = GMSMarker()
//        marker.position = coordinate
//        marker.map = PVC?.mapView
        PVC?.resetMarkers()
        PVC?.setMarker1(Lat: coordinate.latitude, Long: coordinate.longitude, Title: nil, Snippet: nil, imageString: nil, map: PVC!.mapView)
    }
    @IBAction func switchSearchOption(_ sender: Any) {
        if isSearching == true {
            if routeSelectSegmentControl.selectedSegmentIndex == 0 {
                findShortestRoute()
            }
            else if routeSelectSegmentControl.selectedSegmentIndex == 1 {
                findFastesRoute()
            }
        }
    }
    
}
extension SearchDirectionViewController: GMSAutocompleteViewControllerDelegate {
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    // Get the place name from 'GMSAutocompleteViewController'
    // Then display the name in textField
    let PVC = parent as! MapViewController
    switch textfieldTapped {
    case 0:
        txtFrom.text = place.name
        fLat = place.coordinate.latitude
        fLong = place.coordinate.longitude
        PVC.placeMarker1.map = PVC.mapView
        PVC.placeMarker1.position = CLLocationCoordinate2D(latitude: self.fLat, longitude: self.fLong)
    case 1:
        txtTo.text = place.name
        tLat = place.coordinate.latitude
        tLong = place.coordinate.longitude
        PVC.placeMarker2.map = PVC.mapView
        PVC.placeMarker2.position = CLLocationCoordinate2D(latitude: self.tLat, longitude: self.tLong)
    default:
        break;
    }
    PVC.mapView.animate(toLocation: place.coordinate)
    PVC.mapView.animate(toZoom: 16)

// Dismiss the GMSAutocompleteViewController when something is selected
    dismiss(animated: true, completion: nil)
  }
func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // Handle the error
    print("Error: ", error.localizedDescription)
  }
func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    // Dismiss when the user canceled the action
    dismiss(animated: true, completion: nil)
  }
func getDirection(sLat:Double,sLong:Double,eLat:Double,eLong:Double,mode:String){
    let PVC = parent as! MapViewController
    PVC.mapView.animate(toZoom: 10)
    let start = CLLocationCoordinate2D(latitude: sLat, longitude: sLong)
    let end = CLLocationCoordinate2D(latitude: eLat, longitude: eLong)
    let url = "https://maps.googleapis.com/maps/api/directions/json?"
    let origin = "origin=\(sLat),\(sLong)"
    let destination = "destination=\(eLat),\(eLong)"
    let key = "key=AIzaSyBy9V-ZUsws7Gf1BoYDacA_RCFq5aUMtLQ"
    let mode = "mode=\(mode)"
    let request = "\(url)\(origin)&\(destination)&\(mode)&\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    AF.request(request!,method:.post)
                .validate()
                .responseJSON { response in
                    // 3 - HTTP response handle
                    self.setMarker(coordinate: start)
                    self.setMarker(coordinate: end)
                    let json = JSON(response.data!)
                    let routes = json["routes"].arrayValue
                    print(routes)
                    for route in routes
                    {
                        let routeOverviewPolyline = route["overview_polyline"].dictionary
                        let points = routeOverviewPolyline?["points"]?.stringValue
                        let path = GMSPath.init(fromEncodedPath: points!)
                        let polyline = GMSPolyline.init(path: path)
                        let bounds = GMSCoordinateBounds(path: path!)
                        PVC.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                        polyline.strokeColor = UIColor.blue
                        polyline.strokeWidth = 2
                        polyline.map = PVC.mapView
                    }
        }
    }
}

