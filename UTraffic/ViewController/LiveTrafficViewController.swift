//
//  LiveTrafficViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 12/15/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit

class LiveTrafficViewController: UIViewController {

    @IBOutlet weak var liveTrafficLabel: UILabel!
    @IBOutlet weak var liveTrafficSwitch: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()

        liveTrafficSwitch.addTarget(self, action: #selector(self.switchValueDidChange), for: .valueChanged)
        // Do any additional setup after loading the view.
    }
    @objc func switchValueDidChange(sender:UISwitch!) {
        let PVC = parent as? MapViewController
        if sender.isOn == true {
            PVC?.showStatusPolylines()
            print("Live Mode On")
        }
        else{
            PVC?.hideStatusPolylines()
            print("Live Mode Off")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        liveTrafficLabel.text = "LiveTrafficKey".localized
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
