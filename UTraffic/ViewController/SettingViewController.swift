//
//  SettingViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 10/4/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    @IBOutlet weak var changeLangLabel: UILabel!
    @IBOutlet weak var AQISystemLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var aqiSegmentsControl: UISegmentedControl!
    @IBOutlet weak var langSegmentsControl: UISegmentedControl!
    @IBOutlet weak var tempSegmentsControl: UISegmentedControl!
    @IBOutlet weak var evaluateModeSegmentsControl: UISegmentedControl!
    @IBOutlet weak var evaluationModeLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        changeLangLabel.text = "ChangeLanguageKey".localized
        AQISystemLabel.text = "AQISystemKey".localized
        tempLabel.text = "TemperatureKey".localized

        evaluationModeLabel.text = "EvaluationModeKey".localized
        evaluateModeSegmentsControl.setTitle("OffKey".localized, forSegmentAt: 0)
        evaluateModeSegmentsControl.setTitle("OnKey".localized, forSegmentAt: 1)
        switch UserDefaults.standard.string(forKey: "utraffic_language") {
        case "vi":
            langSegmentsControl.selectedSegmentIndex = 0
        case "en":
            langSegmentsControl.selectedSegmentIndex = 1
        default:
            langSegmentsControl.selectedSegmentIndex = 1
        }
        switch UserDefaults.standard.string(forKey: "aqiType"){
        case "us":
            aqiSegmentsControl.selectedSegmentIndex = 0
        case "cn":
            aqiSegmentsControl.selectedSegmentIndex = 1
        default:
            aqiSegmentsControl.selectedSegmentIndex = 0
        }
        switch UserDefaults.standard.string(forKey: "tempType") {
        case "c":
            tempSegmentsControl.selectedSegmentIndex = 0
        case "f":
            tempSegmentsControl.selectedSegmentIndex = 1
        default:
            tempSegmentsControl.selectedSegmentIndex = 0
        }

        switch UserDefaults.standard.bool(forKey: "evaluationMode"){
        case true:
            evaluateModeSegmentsControl .selectedSegmentIndex = 1
        case false:
            evaluateModeSegmentsControl.selectedSegmentIndex = 0
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
         AppUtility.lockOrientation(.all)
    }
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    func changeLanguage(code:String){

    }
    @IBAction func saveSettings(_ sender: Any) {
        var isSettingChanged = false
        var isLanguageChanged = false
        let currentAQIType = UserDefaults.standard.string(forKey: "aqiType")
        switch aqiSegmentsControl.selectedSegmentIndex {
        case 0:
            if currentAQIType != "us"{
                UserDefaults.standard.set("us", forKey: "aqiType")
                isSettingChanged = true
            }
        case 1:
            if currentAQIType != "cn" {
                UserDefaults.standard.set("cn", forKey: "aqiType")
                isSettingChanged = true
            }
        default:
             UserDefaults.standard.set("us", forKey: "aqiType")
        }
        
        let currentTempType = UserDefaults.standard.string(forKey: "tempType")
        switch tempSegmentsControl.selectedSegmentIndex {
        case 0:
            if currentTempType != "c"{
                UserDefaults.standard.set("c", forKey: "tempType")
                isSettingChanged = true
            }
        case 1:
            if currentTempType != "f"{
                UserDefaults.standard.set("f", forKey: "tempType")
                isSettingChanged = true
            }
        default:
             UserDefaults.standard.set("c", forKey: "tempType")
        }
        
        let currentLangCode = UserDefaults.standard.string(forKey: "utraffic_language")
        switch langSegmentsControl.selectedSegmentIndex {
        case 0:
            if currentLangCode != "vi"{
                UserDefaults.standard.set("vi", forKey: "utraffic_language")
                isLanguageChanged = true
            }
        case 1:
            if currentLangCode != "en"{
                UserDefaults.standard.set("en", forKey: "utraffic_language")
                isLanguageChanged = true
            }
        default: break
        }
        
        let currentEvaluationMode = UserDefaults.standard.bool(forKey: "evaluationMode")
        switch evaluateModeSegmentsControl.selectedSegmentIndex{
        case 0:
            if currentEvaluationMode == true {
                UserDefaults.standard.set(false,forKey: "evaluationMode")
                isSettingChanged = true
            }
        case 1:
            if currentEvaluationMode == false {
                UserDefaults.standard.set(true,forKey: "evaluationMode")
                isSettingChanged = true
            }
        default:
            UserDefaults.standard.set(false,forKey: "evaluationMode")
        }
        if isLanguageChanged == true {
            let message = "AskToRestartKey".localized
            let confirmAlertCtrl = UIAlertController(title:"RestartKey".localized  , message: message, preferredStyle: .alert)

            let confirmAction = UIAlertAction(title:"CloseKey".localized , style: .destructive) { _ in
                exit(EXIT_SUCCESS)
            }
            confirmAlertCtrl.addAction(confirmAction)

            let cancelAction = UIAlertAction(title: "CancelKey".localized, style: .cancel, handler: nil)
            confirmAlertCtrl.addAction(cancelAction)
            present(confirmAlertCtrl, animated: true, completion: nil)
        }
        else{
            if isSettingChanged == true{
                displaySuccess(title: "SuccessKey".localized, message: "SuccessSettingKey".localized, viewcontroller: self)
            }
            else{
                displayAlert(title: "ErrorKey".localized, message: "NothingChangedKey".localized, viewcontroller: self)
            }
        }

        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
