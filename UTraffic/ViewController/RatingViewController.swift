//
//  RatingViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 12/1/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit
import HCSStarRatingView
class RatingViewController: UIViewController {

    var reportId = String()
    var index = Int()
    @IBOutlet weak var starsView: HCSStarRatingView!
    @IBOutlet weak var btnEvaluate: RoundButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        starsView.filledStarImage = UIImage(named: "star.fill")?.withRenderingMode(.alwaysTemplate)
        starsView.halfStarImage = UIImage(named: "star.lefthalf.fill")?.withRenderingMode(.alwaysTemplate)
        starsView.emptyStarImage = UIImage(named: "star")?.withRenderingMode(.alwaysTemplate)
        starsView.emptyStarColor = .systemYellow
        starsView.tintColor = .systemYellow
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    @IBAction func evaluateReports(_ sender: Any) {
        var start1 = NSDate()
        
        let PVC = parent as! ReportDetailViewController
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        if accessToken == nil || accessToken == ""{
            displayAlert(title: "ErrorKey".localized, message: "AskToLogInKey".localized, viewcontroller: self)
            return
        }
        let score = starsView.value*2/10
        //print(score)
        var end1 = NSDate()
        evaluateReport(accessToken: accessToken!, reportId: reportId, score: Float(score)){
            (isSuccess,message) in
            end1 = NSDate()
            if isSuccess == true {
                let alert = UIAlertController(title: "SuccessKey".localized, message: "SuccessEvaluateKey".localized, preferredStyle: UIAlertController.Style.alert)
                // add an action (button)
                alert.addAction(UIAlertAction(title: "ContinueKey".localized, style: UIAlertAction.Style.default, handler: {action in
                    PVC.isValidated[self.index] = true
                    PVC.setView(view: PVC.ratingView, isHidden: true)
                }))
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
            else{
                displayAlert(title: "ErrorKey".localized, message: message!, viewcontroller: self)
            }
            print("----------------------------------------------------")
            print("Total Evaluation time:",end1.timeIntervalSince(start1 as Date))
            print("----------------------------------------------------")
        }
    }
    @IBAction func exitBtn(_ sender: Any) {
        let PVC = parent as! ReportDetailViewController
        PVC.setView(view: PVC.ratingView, isHidden: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
