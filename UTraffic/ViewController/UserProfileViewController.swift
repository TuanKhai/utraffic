//
//  UserProfileViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 10/12/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit
class UserProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate {

    @IBOutlet weak var txtFullName: DesignableUITextField!
    @IBOutlet weak var txtPhoneNumber: DesignableUITextField!
    @IBOutlet weak var txtEmail: DesignableUITextField!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var profilePicture: UIRoundImage!
    var name = String()
    var email = String()
    var phone = String()
    var imageURL = String()
    var curLat = Double()
    var curLong = Double()
    var imageChanged = Bool()
    var imageDir = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        imageChanged = false
        // Do any additional setup after loading the view.
    }
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait)
        print("viewWillAppear")
        if imageChanged != true {
            name = UserDefaults.standard.string(forKey: "name")!
            txtFullName.text = name
            phone = UserDefaults.standard.string(forKey: "phone")!
            txtPhoneNumber.text = phone
            email = UserDefaults.standard.string(forKey: "email")!
            txtEmail.text = email
            
            txtFullName.placeholder = "FullNameKey".localized
            txtEmail.placeholder = "EmailKey".localized
            txtPhoneNumber.placeholder = "PhoneNumberKey".localized
            btnEdit.setTitle("EditKey".localized, for: .normal)
            btnUpdate.setTitle("UpdateKey".localized, for: .normal)
            
            imageURL = UserDefaults.standard.string(forKey: "avatarURL")!
            if imageURL != "" {
                profilePicture.load(url: URL(string: imageURL)!)
                profilePicture.clipsToBounds = true
            }
            
             let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(imageLongPressed(longPressGesture:)))
            profilePicture.isUserInteractionEnabled = true
            profilePicture.addGestureRecognizer(longPressGestureRecognizer)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
        print("aaaa")
    }
    @objc func imageLongPressed(longPressGesture: UILongPressGestureRecognizer)
    {
        let tappedImage = longPressGesture.view as! UIImageView
        tappedImage.previewImage(viewController: self)
        // Your action
    }
     @IBAction func editProfilePicture(_ sender: Any) {
         let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
         alert.addAction(UIAlertAction(title: "TakePictureKey".localized, style: .default , handler:{ (UIAlertAction)in
             print("Camera")
            self.imageChanged = true
             self.getImage(fromSourceType: .camera)
         }))

         alert.addAction(UIAlertAction(title: "ChooseFromLibraryKey".localized, style: .default , handler:{ (UIAlertAction)in
             print("Library")
            self.imageChanged = true
             self.getImage(fromSourceType: .photoLibrary)
         }))
        
        alert.addAction(UIAlertAction(title: "ResetDefaultKey".localized, style: .default , handler:{ (UIAlertAction)in
            print("Reset Default")
            self.imageURL = UserDefaults.standard.string(forKey: "avatarURL")!
            if self.imageURL != "" {
                self.profilePicture.load(url: URL(string: self.imageURL)!)
                self.profilePicture.clipsToBounds = true
            }
            self.imageChanged = false
        }))
        
        alert.addAction(UIAlertAction(title: "RemoveImageKey".localized, style: .default , handler:{ (UIAlertAction)in
            print("Remove image")
            resetPicture(imageView: self.profilePicture)
            self.imageChanged = true
        }))
        

         alert.addAction(UIAlertAction(title: "CancelKey".localized, style: .cancel, handler:{ (UIAlertAction)in
             print("User click Dismiss button")
         }))

         self.present(alert, animated: true, completion: {
             print("completion block")
         })
     }
     func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
         //Check is source type available
         if UIImagePickerController.isSourceTypeAvailable(sourceType) {
             let imagePickerController = UIImagePickerController()
             imagePickerController.delegate = self
             imagePickerController.sourceType = sourceType
             imagePickerController.allowsEditing = true
             self.present(imagePickerController, animated: true, completion: nil)
         }
     }
 func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imageChanged = false
    }
      func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
          if let editedImage = info[.editedImage] as? UIImage {
            profilePicture.image = editedImage
              profilePicture.clipsToBounds = true
              picker.dismiss(animated: true, completion: nil)
          } else if let originalImage = info[.originalImage] as? UIImage {
              profilePicture.image = originalImage
              profilePicture.clipsToBounds = true
              picker.dismiss(animated: true, completion: nil)
          }

      }
    @IBAction func updateUser(_ sender: Any) {
        let validation = validate()
        if checkForChanges() == false {
            displayAlert(title: "WarningKey".localized, message: "NoChangeKey".localized, viewcontroller: self)
            return
        }
        else{
            if validation.pass == true {
                let accessToken = UserDefaults.standard.string(forKey: "accessToken")
                self.name = self.txtFullName.text!
                self.email = self.txtEmail.text!
                self.phone = self.txtPhoneNumber.text!
                let dispatchGroup = DispatchGroup()
                dispatchGroup.enter()
                showIndicator(text: "LoadingKey".localized, view: self.view)
                if imageChanged == true {
                    uploadFile(image: profilePicture){[weak self](isSuccess,message,data) in
//                        do {
//                            try FileManager.default.removeItem(at: URL(string: self!.imageDir)!)
//                            print("Image deleted")
//                        } catch let error as NSError {
//                            print("Error: \(error.domain)")
//                        }
                        FileManager.default.clearTmpDirectory()
                        if isSuccess == true{
                            self!.imageURL = data!
                        }
                        else{
                            displayAlert(title: "ErrorKey".localized, message: message!, viewcontroller: self!)
                            dispatchGroup.leave()
                            return
                        }
                        dispatchGroup.leave()
                    }
                }
                else{
                    dispatchGroup.leave()
                }
                dispatchGroup.notify(queue: .main){
                    patchCurrentUserInfo(accessToken: accessToken!, name:self.name , email:self.email , phone:self.phone , avatar: self.imageURL){[weak self](isSuccess,message) in
                        hideIndicator(view: self!.view)
                        if isSuccess == true {
                            self!.afterUpdate(accessToken: accessToken!)
                            
                        }else {
                            displayAlert(title: "ErrorKey".localized, message: message!, viewcontroller: self!)
                        }
                    }
                }
            }
            else{
                displayAlert(title: "ErrorKey".localized, message: validation.message!, viewcontroller: self)
            }
        }
    }
    func validate()->(pass:Bool,message:String?){
        if txtFullName.text == ""{
            return (false,"EmptyNameKey".localized)
        }
        if txtFullName.text!.count < 4{
            return (false,"ShortNameKey".localized)
        }
        if txtEmail.text == ""{
            return (false,"EmptyEmailKey".localized)
        }
        if validateEmail(emailString: txtEmail.text!) == false {
            return (false,"WrongFormatEmailKey".localized)
        }
        if txtPhoneNumber.text == ""{
            return (false,"EmptyPhoneKey".localized)
        }
        if txtPhoneNumber.text!.count < 10{
            return (false,"ShortPhoneKey".localized)
        }
        return (true,nil)
    }
    func afterUpdate(accessToken:String){
        showIndicator(text: "LoadingKey".localized, view: self.view)
        getCurrentUserInfo(accessToken: accessToken){(isSuccess,message) in
            hideIndicator(view: self.view)
            if isSuccess == true {
                displaySuccess(title: "SuccessKey".localized, message: "SuccessUpdateKey".localized, viewcontroller: self)
            }
            else if isSuccess == false{
                resetUserDefault()
                displayAlert(title: "ErrorKey".localized, message: message!, viewcontroller: self)
            }
        }
    }
    func checkForChanges()->(Bool){
        if imageChanged == true ||
            txtFullName.text != UserDefaults.standard.string(forKey: "name") ||
            txtPhoneNumber.text != UserDefaults.standard.string(forKey: "phone") ||
            txtEmail.text != UserDefaults.standard.string(forKey: "email") {
            return true
        }
        return false
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
