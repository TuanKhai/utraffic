//
//  MusicPlayerViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 11/10/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit
import MediaPlayer
class MusicPlayerViewController: UIViewController,MPMediaPickerControllerDelegate {

    @IBOutlet weak var openButton: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var albumImage: UIImageView!
    @IBOutlet weak var labelArtist: UILabel!
    let mp = MPMusicPlayerController.systemMusicPlayer
    var timer = Timer()
    var isPlaying = Bool()
    var mediapicker1: MPMediaPickerController!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mediaPicker: MPMediaPickerController = MPMediaPickerController.self(mediaTypes:MPMediaType.music)
        mediaPicker.allowsPickingMultipleItems = true
        mediapicker1 = mediaPicker
        mediaPicker.delegate = self
        
        isPlaying = false
        mp.prepareToPlay()
        // Do any additional setup after loading the view.
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.timerFired(_:)), userInfo: nil, repeats: true)
        self.timer.tolerance = 0.1
        
        mp.beginGeneratingPlaybackNotifications()
        NotificationCenter.default.addObserver(self, selector:#selector(self.updateNowPlayingInfo), name: NSNotification.Name.MPMusicPlayerControllerNowPlayingItemDidChange, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        labelTitle.alpha = 0
        labelArtist.alpha = 0
        AppUtility.lockOrientation(.portrait)
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    @objc func timerFired(_:AnyObject) {
        if let currentTrack = MPMusicPlayerController.systemMusicPlayer.nowPlayingItem {
            let trackName = currentTrack.title!
             let trackArtist = currentTrack.artist!
            let image = currentTrack.artwork?.image(at: albumImage.frame.size)
            labelArtist.alpha = 1
            labelTitle.alpha = 1
            labelTitle.text = trackName
            labelArtist.text = trackArtist
            albumImage.image = image
            albumImage.layer.masksToBounds = true
        }
    
    }
    @objc func updateNowPlayingInfo(){
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.timerFired(_:)), userInfo: nil, repeats: true)
     self.timer.tolerance = 0.1
    }
    @IBAction func playPause(_ sender: Any) {
        if isPlaying == false {
            mp.play()
            btnPlay.setImage(UIImage(named: "pause.fill"), for: .normal)
            isPlaying  = true
        }
        else if isPlaying == true {
            mp.pause()
            btnPlay.setImage(UIImage(named: "play.fill"), for: .normal)
            isPlaying = false
        }
    }
    @IBAction func Previous(_ sender: Any) {
        let trackElapsed = mp.currentPlaybackTime
        if trackElapsed < 3 {
            mp.skipToPreviousItem()
        } else {
            mp.skipToBeginning()
        }
    }
    @IBAction func Next(_ sender: Any) {
        mp.skipToNextItem()
    }
    @IBAction func musicList(_ sender: Any) {
        self.present(mediapicker1, animated: true, completion: nil)
    }
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        self.dismiss(animated: true, completion: nil)
        let selectedSongs = mediaItemCollection
        mp.setQueue(with: selectedSongs)
        mp.play()
        btnPlay.setImage(UIImage(named: "pause.fill"), for: .normal)
        isPlaying  = true
        
    }
    @IBAction func openMusicContainer(_ sender: Any) {
        let PVC = parent as! MapViewController
        if PVC.musicShowing == true {
            PVC.closeMusicContainer(button: openButton)
        }
        else {
            PVC.openMusicContainer(button: openButton)
        }
       
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
