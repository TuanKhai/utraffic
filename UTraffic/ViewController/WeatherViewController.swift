//
//  WeatherViewController.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 11/7/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class WeatherViewController: UIViewController {
    var weatherTimer: Timer?

    @IBOutlet weak var weatherBackground: UIRoundView!
    @IBOutlet weak var weatherSymbol: UIImageView!
    @IBOutlet weak var txtTemp: UILabel!
    @IBOutlet weak var txtAQI: UILabel!
    @IBOutlet weak var aqiBackground: UIRoundView!
    override func viewDidLoad() {
        super.viewDidLoad()
        stopTimer()
        weatherTimer = Timer.scheduledTimer(timeInterval: 300, target: self, selector: #selector(self.getWeather), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        stopTimer()
    }
    override func viewDidAppear(_ animated: Bool) {
        getWeather()
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppUtility.lockOrientation(.all)
    }
    override func viewWillAppear(_ animated: Bool) {
        AppUtility.lockOrientation(.portrait) 
    }
    @objc func getWeather(){
        let PVC = parent as! MapViewController
        openWeatherMap(lat: PVC.currentLat, long: PVC.currentLong) { [weak self](isSuccess, aqicn, aqius, wic, temp) in
            if isSuccess == true{
                var aqi = String()
                var tempurature = String()
                let aqiType = UserDefaults.standard.string(forKey: "aqiType")
                let temperatureType = UserDefaults.standard.string(forKey: "tempType")
                switch aqiType {
                case "cn":
                    aqi = aqicn!
                case "us":
                    aqi = aqius!
                default:
                    aqi = aqius!
                }
                switch temperatureType {
                case "c":
                    tempurature = "\(temp!)°C"
                case "f":
                    tempurature = "\(self!.celsiusToFahrenheit(c: Int(temp!)!))°F"
                default:
                    tempurature = "\(temp!)°C"
                }
                let symbol = self!.getSymbol(id: wic!)
                let hexString = self!.getAQIcolor(aqi: aqi)
                let aqiColor = UIColor(hexString: hexString)
                self!.weatherBackground.backgroundColor = aqiColor
                self!.aqiBackground.backgroundColor = aqiColor
                self!.txtTemp.text = tempurature
                self!.txtTemp.textColor = aqiColor
                self!.txtAQI.text = aqi
                self!.weatherSymbol.image = symbol
            }
            else if isSuccess == false{
                print("Cannot get value")
            }
        }
    }
    func stopTimer(){
        weatherTimer?.invalidate()
    }

    func getSymbol(id:String) -> UIImage {
        var wic = UIImage()
        switch id {
        case "01d":
            wic = UIImage(named: "sun.max")!
        case "01n":
            wic = UIImage(named: "moon.stars")!
        case "02d":
            wic = UIImage(named: "cloud.sun")!
        case "02n":
            wic = UIImage(named: "cloud.moon")!
        case "03d":
            wic = UIImage(named: "cloud")!
        case "03n":
            wic = UIImage(named: "cloud.fill")!
        case "04d":
            wic = UIImage(named: "smoke")!
        case "04n":
            wic = UIImage(named: "smoke.fill")!
        case "09d":
            wic = UIImage(named: "cloud.rain")!
        case "09n":
            wic = UIImage(named: "cloud.rain.fill")!
        case "10d":
            wic = UIImage(named: "cloud.sun.rain")!
        case "10n":
            wic = UIImage(named: "cloud.moon.rain")!
        case "11d":
            wic = UIImage(named: "cloud.bolt")!
        case "13d":
            wic = UIImage(named: "cloud.snow")!
        case "50d":
            wic = UIImage(named: "cloud.fog")!
        default:
            wic = UIImage(named: "sun.max")!
        }
        return wic
    }
    func getAQIcolor(aqi:String) -> String{
        let AQI = Int(aqi)
        switch AQI! {
        case 0...50:
            return aqiColor.green.rawValue
        case 51...100:
            return aqiColor.yellow.rawValue
        case 101...150:
            return aqiColor.orange.rawValue
        case 151...200:
            return aqiColor.red.rawValue
        case 201...300:
            return aqiColor.purple.rawValue
        case 300...500:
            return aqiColor.darkPurple.rawValue
        default:
            return aqiColor.green.rawValue
        }

        
    }
    func celsiusToFahrenheit(c:Int) -> Int{
        return Int((c * 9/5) + 32)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
