//
//  extensions.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 9/11/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import Foundation
import MBProgressHUD
import UIKit
import CoreLocation
extension FloatingPoint {
    func rounded(to n: Int) -> Self {
        let n = Self(n)
        return (self / n).rounded() * n
    }
}
extension UIViewController {
    var appDelegate: AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
   }
}
extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
    func previewImage(viewController: UIViewController){
        let vc = UIStoryboard(name: "SpectatePicture", bundle: nil).instantiateViewController(withIdentifier: "SpectatePictureViewController") as! SpectateImageViewController
        vc.image = self.image!
        viewController.present(vc, animated: true, completion: nil)

    }
}
extension UserDefaults {
    func contains(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
}

extension UIApplication {
    class var statusBarBackgroundColor: UIColor? {
        get {
            return (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor
        } set {
            (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor = newValue
        }
    }
    
    func applicationBundleIdentifier() -> String {
        return Bundle.main.bundleIdentifier!
    }
    
    func applicationVersion() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    func applicationBuild() -> String {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    
    func versionBuild() -> String {
        let version = self.applicationVersion()
        let build = self.applicationBuild()
        
        return "v\(version) (\(build))"
    }
    
    func showProcessing(_ message: String = "") {
        let window = UIApplication.shared.windows.last
        let hud = MBProgressHUD.showAdded(to: window!, animated: true)
        
        if message.count > 0 {
            hud.mode = MBProgressHUDMode.text
            hud.label.text = message
            hud.hide(animated: true, afterDelay: 0.7)
        }
        
        hud.isUserInteractionEnabled = false
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func hideProcessing() {
        let window = UIApplication.shared.windows.last
        MBProgressHUD.hide(for: window!, animated: true)
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}
extension CLLocationCoordinate2D: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(Int(latitude * 1000))
    }

    static public func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        // Due to the precision here you may wish to use alternate comparisons
        // The following compares to less than 1/100th of a second
        // return abs(rhs.latitude - lhs.latitude) < 0.000001 && abs(rhs.longitude - lhs.longitude) < 0.000001
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
}
extension String {
    var localized: String {
        if let _ = UserDefaults.standard.string(forKey: "utraffic_language") {} else {
            // we set a default, just in case
            UserDefaults.standard.set("en", forKey: "utraffic_language")
        }

        let lang = UserDefaults.standard.string(forKey: "utraffic_language")

        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)

        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}
func displayAlert(title:String?,message:String,viewcontroller:UIViewController){
    // create the alert
     let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
     // add an action (button)
     alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
     // show the alert
     viewcontroller.present(alert, animated: true, completion: nil)
}
func displaySuccess(title:String,message:String,viewcontroller:UIViewController){
    // create the alert
     let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
     // add an action (button)
     alert.addAction(UIAlertAction(title: "ContinueKey".localized, style: UIAlertAction.Style.default, handler: {action in
        viewcontroller.dismiss(animated: true, completion: nil)
     }))
     // show the alert
     viewcontroller.present(alert, animated: true, completion: nil)
}
func showIndicator(text:String,view:UIView){
    let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.indeterminate
      loadingNotification.label.text = "Loading"
}
func hideIndicator(view:UIView){
    MBProgressHUD.hideAllHUDs(for: view, animated: true)
}
func resetPicture(imageView:UIImageView){
    imageView.image = UIImage(named:"person.crop.circle.fill")
    imageView.clipsToBounds = true
}

func validateEmail(emailString: String) -> Bool {
 let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: emailString)
}
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
func resetUserDefault(){
    UserDefaults.standard.set(false, forKey: "isLoggedIn")
    UserDefaults.standard.removeObject(forKey: "name")
    UserDefaults.standard.removeObject(forKey: "email")
    UserDefaults.standard.removeObject(forKey: "avatarURL")
    UserDefaults.standard.removeObject(forKey: "accessToken")
    UserDefaults.standard.removeObject(forKey: "hasServerError")
    UserDefaults.standard.synchronize()
}
func checkMaxLength(textField: UITextField!, maxLength: Int) {
    if (textField.text!.count > maxLength) {
        textField.deleteBackward()
    }
}
class ProfanityFilter: NSObject {

    static let sharedInstance = ProfanityFilter()
    private override init() {}

    // Customize as needed

    private let dirtyWords = "\\b(ducker|mother ducker|motherducker|shot|bad word|another bad word|)\\b"

 
    private func matches(for regex: String, in text: String) -> [String] {

        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let nsString = text as NSString
            let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }

    public func cleanUp(forString: String,inString:String) -> String {
        let dirtyWords = matches(for: forString, in: inString)

        if dirtyWords.count == 0 {
            return inString
        } else {
            var newString = inString

            dirtyWords.forEach({ dirtyWord in
                let newWord = String(repeating: "*", count: dirtyWord.count)
                newString = newString.replacingOccurrences(of: dirtyWord, with: newWord, options: [.caseInsensitive])
            })

            return newString
        }
    }
    
}
struct AppUtility {

    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {

        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }

    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {

        self.lockOrientation(orientation)

        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }

}
func getDocumentDirectoryPath() -> NSString {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0]
    return documentsDirectory as NSString
}
func saveImageToDocumentsDirectory(image: UIImage, withName: String) -> String? {
    if let data = image.pngData() {
        let dirPath = getDocumentDirectoryPath()
        let imageFileUrl = URL(fileURLWithPath: dirPath.appendingPathComponent(withName) as String)
        do {
            try data.write(to: imageFileUrl)
            print("Successfully saved image at path: \(imageFileUrl)")
            return imageFileUrl.absoluteString
        } catch {
            print("Error saving image: \(error)")
        }
    }
    return nil
}
func loadImageFromDocumentsDirectory(imageName: String) -> UIImage? {
    let tempDirPath = getDocumentDirectoryPath()
    let imageFilePath = tempDirPath.appendingPathComponent(imageName)
    return UIImage(contentsOfFile:imageFilePath)
}
func getTempDirectory() -> NSString{
    let path = NSTemporaryDirectory() as NSString
    return path
}
func saveImageToTempDirectory(image: UIImage, withName: String) -> String? {
    if let data = image.pngData() {
        let dirPath = getTempDirectory()
        let imageFileUrl = URL(fileURLWithPath: dirPath.appendingPathComponent(withName) as String)
        do {
            try data.write(to: imageFileUrl)
            print("Successfully saved image at path: \(imageFileUrl)")
            return imageFileUrl.absoluteString
        } catch {
            print("Error saving image: \(error)")
        }
    }
    return nil
}
func loadImageFromTempDirectory(imageName: String) -> UIImage?{
    let tempDirPath = getTempDirectory()
    let imageFilePath = tempDirPath.appendingPathComponent(imageName)
    return UIImage(contentsOfFile:imageFilePath)
}
extension FileManager {
    func clearTmpDirectory() {
        do {
            let tmpDirectory = try contentsOfDirectory(atPath: NSTemporaryDirectory())
            try tmpDirectory.forEach {[unowned self] file in
                let path = String.init(format: "%@%@", NSTemporaryDirectory(), file)
                try self.removeItem(atPath: path)
            }
        } catch {
            print(error)
        }
    }
}





