import Foundation

enum aqiColor:String {
    case green = "#34C759"
    case yellow = "#F0C808"
    case red = "#E71D36"
    case orange = "#E77245"
    case purple = "#AC81B9"
    case darkPurple = "#535568"
}
let ErrorCode = [500,503,404]

struct API {
    struct server {
        static let base = "https://api.bktraffic.com/api"
    }
    struct extend{
        static let userRegister = "/auth/register"
        static let patchUserAccount = "/user/update-user-info"
        static let googleSignIn = "/auth/login-with-google"
        static let facebookSignIn = "/auth/login-with-facebook"
        static let normalSignIn = "/auth/login"
        static let userInfo = "/user/get-user-info"
    }
}

let filterWordsVN = ["đụ","mày","tao","đĩ","cặc","cứt"]
let filterWordsUS = ["fuck","fucking","dick","cunt","whore","shit"]

