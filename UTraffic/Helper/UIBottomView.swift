//
//  UISIdeMenu.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 9/10/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import UIKit

@IBDesignable
class UIBottomView: UIView {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
            self.clipsToBounds = true
            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            self.layer.masksToBounds = false
            self.layer.shadowRadius = 7.0
            self.layer.shadowOpacity = 0.2
            /*
             layerMaxXMaxYCorner – lower right corner
             layerMaxXMinYCorner – top right corner
             layerMinXMaxYCorner – lower left corner
             layerMinXMinYCorner – top left corner
             */
            
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
}
