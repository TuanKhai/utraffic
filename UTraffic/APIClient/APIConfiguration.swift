//
//  APIConfiguration.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 10/5/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import Foundation
import Alamofire
protocol APIConfiguration: URLRequestConvertible {
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters? { get }
}
