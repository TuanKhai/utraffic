//
//  APIClient.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 10/4/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import Foundation
import Alamofire
class APIClient{
        static func login(username: String, password: String, completion:@escaping (Result<ApiResponse>)->Void) {
            AF.request(APIRouter.login(username: username, password: password))
                     .responseDecodable { (response: DataResponse<ApiResponse>) in
                completion(response.result)
            }
        }

}

