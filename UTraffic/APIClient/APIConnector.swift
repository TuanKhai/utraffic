//
//  APIConnector.swift
//  UTraffic
//
//  Created by Do Huynh Tuan Khai on 10/15/19.
//  Copyright © 2019 Do Huynh Tuan Khai. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation
func userRegister(username:String,password:String,name:String?,email:String?,phone:String?,avatar:String?,completionHandler: @escaping (Bool,String?) ->()){
    var message = ""
    let URL:String = "https://api.bktraffic.com/api/auth/register"
    var param: Parameters
    param = ["username":username,"password":password]
    if name != nil{
        param["name"] = name
    }
    if email != nil{
        param["email"] = email
    }
    if phone != nil{
        param["phone"] = phone
    }
    if avatar != nil{
        param["avatar"] = avatar
    }
    AF.request(URL, method: .post, parameters: param).validate().responseJSON { response in
        if response.result.isSuccess{
            let json = JSON(response.result.value!)
            print(json)
            if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
                message = "ServerErrorKey".localized
                completionHandler(false,message)
            }
            else if json["code"].stringValue == "400"{
                
                let err = json["errors"]
                var reason = err[0]["reason"].stringValue
                var fields = err[0]["domain"].stringValue
                if reason == "duplicated"{
                    reason = "ExistedKey".localized
                }
                if fields == "username"{
                    fields = "UsernameKey".localized
                }
                message = "\(fields) \(reason)"
                print(message)
                completionHandler(false,message)
            }
            else if json["code"].stringValue == "200"{
                completionHandler(true,nil)
            }
        }
        else {
            if let data = response.data{
                let err = JSON(data)
                message = err["message"].string!
                completionHandler(false,message)
            }
            if let error = response.result.error{
                if error._code == NSURLErrorTimedOut{
                    message = "RequestTimeOutKey".localized
                    completionHandler(false,message)
                }
            }
        }
    }
}

func patchCurrentUserInfo(accessToken:String,name:String?,email:String?,phone:String?,avatar:String?,completionHandler: @escaping (Bool,String?) ->()){
    var message = ""
    let URL:String = "https://api.bktraffic.com/api/user/update-user-info"
    var param: Parameters
    param = [:]
    let headers: HTTPHeaders = [
        "Authorization": "Bearer \(accessToken)",
        "Accept": "application/json"
    ]
    if name != nil{
        param["name"] = name
    }
    if email != nil{
        param["email"] = email
    }
    if phone != nil{
        param["phone"] = phone
    }
    if avatar != nil{
        param["avatar"] = avatar
    }
    AF.request(URL, method: .post, parameters: param,headers: headers).validate().responseJSON { response in
    // 3 - HTTP response handle
        if response.result.isSuccess{
            let json = JSON(response.result.value!)
            if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
                message = "ServerErrorKey".localized
                completionHandler(false,message)
            }
            else if json["code"].stringValue == "200"{
                completionHandler(true,nil)
            }
        }
        else {
            if let data = response.data{
                let err = JSON(data)
                message = err["message"].string!
                completionHandler(false,message)
            }
            if let error = response.result.error{
                if error._code == NSURLErrorTimedOut{
                    message = "RequestTimeOutKey".localized
                    completionHandler(false,message)
                }
            }
        }
    }
}
func googleSignIn(userId:String,token: String,completionHandler: @escaping (Bool,String?)->()){
    let URL:String = "https://api.bktraffic.com/api/auth/login-with-google"
    var message = ""
    AF.request(URL, method: .post, parameters: ["google_id":userId,"google_token":token]).validate().responseJSON { response in
        if response.result.isSuccess{
            let json = JSON(response.result.value!)
            if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
                message = "ServerErrorKey".localized
                completionHandler(false,message)
            }
            else if json["code"].stringValue == "200"{
                let data = json["data"]
                let accessToken = data["access_token"].string!
                UserDefaults.standard.set(accessToken, forKey: "accessToken")
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                UserDefaults.standard.synchronize()
                completionHandler(true,nil)
            }
        }
        else {
            if let data = response.data{
                let err = JSON(data)
                message = err["message"].string!
                completionHandler(false,message)
            }
            if let error = response.result.error{
                if error._code == NSURLErrorTimedOut{
                    message = "RequestTimeOutKey".localized
                    completionHandler(false,message)
                }
            }
        }
    }
}
func facebookSignIn(userId:String,token: String,completionHandler: @escaping (Bool,String?)->()){
    let URL:String = "https://api.bktraffic.com/api/auth/login-with-facebook"
    // 2 - create request
    var message = ""
    AF.request(URL, method: .post, parameters: ["facebook_id":userId,"facebook_token":token]).validate().responseJSON { response in
        if response.result.isSuccess{
            let json = JSON(response.result.value!)
           
            if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
                message = "ServerErrorKey".localized
                completionHandler(false,message)
            }
            else if json["code"].stringValue == "200"{
                let data = json["data"]
                let accessToken = data["access_token"].string!
                UserDefaults.standard.set(accessToken, forKey: "accessToken")
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                UserDefaults.standard.synchronize()
                completionHandler(true,nil)
            }
        }
        else {
            if let data = response.data{
                let err = JSON(data)
                message = err["message"].string!
                completionHandler(false,message)
            }
            if let error = response.result.error{
                if error._code == NSURLErrorTimedOut{
                    message = "RequestTimeOutKey".localized
                    completionHandler(false,message)
                }
            }
        }
    }
}
func normalLogIn(username:String,password:String,completionHandler:@escaping(Bool,String?)->()){
    let URLString:String = "https://api.bktraffic.com/api/auth/login"
    var message = ""
    // 2 - create request
    AF.request(URLString, method: .post, parameters: ["username":username,"password":password]).validate().responseJSON { response in
        if response.result.isSuccess{
            let json = JSON(response.result.value!)
            if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
                message = "ServerErrorKey".localized
                completionHandler(false,message)
            }
            else if json["code"].stringValue == "200"{
                let data = json["data"]
                let accessToken = data["access_token"].string!
                UserDefaults.standard.set(accessToken, forKey: "accessToken")
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                UserDefaults.standard.synchronize()
                completionHandler(true,nil)
            }
            else if json["code"].stringValue == "400"{
                message = "WrongInfoLogInKey".localized
                completionHandler(false,message)
            }
        }
        else {
            if let data = response.data{
                let err = JSON(data)
                message = err["message"].string!
                completionHandler(false,message)
            }
            if let error = response.result.error{
                if error._code == NSURLErrorTimedOut{
                    message = "RequestTimeOutKey".localized
                    completionHandler(false,message)
                }
            }
        }
    }
}

func getCurrentUserInfo(accessToken:String,completionHandler:@escaping(Bool,String?)->()){
    let URL:String = "https://api.bktraffic.com/api/user/get-user-info"
    let headers: HTTPHeaders = [
        "Authorization": "Bearer \(accessToken)",
        "Accept": "application/json"
    ]
    var message = ""
    AF.request(URL, method: .get,headers: headers).validate().responseJSON { response in
        if response.result.isSuccess{
            let json = JSON(response.result.value!)
            if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
                message = "ServerErrorKey".localized
                completionHandler(false,message)
            }
            else if json["code"].stringValue == "200"{
                let data = json["data"]
                if let fullName = data["name"].string {
                    UserDefaults.standard.set(fullName, forKey: "name")
                }
                else{
                    UserDefaults.standard.set("", forKey: "name")
                }
                if let email = data["email"].string {
                    UserDefaults.standard.set(email, forKey: "email")
                }
                else{
                    UserDefaults.standard.set("", forKey: "email")
                }
                if let phone = data["phone"].string{
                    UserDefaults.standard.set(phone, forKey: "phone")
                }
                else{
                    UserDefaults.standard.set("", forKey: "phone")
                }
                if let imageURL = data["avatar"].string{
                    UserDefaults.standard.set(imageURL,forKey: "avatarURL")
                }
                else{
                    UserDefaults.standard.set("",forKey: "avatarURL")
                }
                if let userID = data["_id"].string{
                    UserDefaults.standard.set(userID,forKey: "userID")
                }
                else {
                    UserDefaults.standard.set("",forKey: "userID")
                }
                UserDefaults.standard.synchronize()
                completionHandler(true,nil)
            }
        }
        else {
            if let data = response.data{
                let err = JSON(data)
                message = err["message"].string!
                completionHandler(false,message)
            }
            if let error = response.result.error{
                if error._code == NSURLErrorTimedOut{
                    message = "RequestTimeOutKey".localized
                    completionHandler(false,message)
                }
            }
        }
    }
}
               
func submitTrafficReportsRemote(accessToken:String,startLat:Double,startLng:Double,endLat:Double,endLng:Double,velocity:String,description:String,causes:[String],images:[UIImageView]?,completionHandler: @escaping (Bool,String?) ->()){
    let URL:String = "https://api.bktraffic.com/api/report/segment/here"
    var imageList = [String]()
    var message = ""
    let dispatchGroup = DispatchGroup()
    var param: Parameters
    param = ["currentLat":startLat,
              "currentLng":startLng,
              "nextLat":endLat,
              "nextLng":endLng,
              "velocity":String(velocity)]
    if description != "" {
        param["description"] = description
    }
    if !causes.isEmpty {
        param["causes"] = causes
    }
    let header: HTTPHeaders = [
         "Authorization": "Bearer \(accessToken)",
         "Accept": "application/json"
     ]
    if !images!.isEmpty{
        var start1 = NSDate()
        var end1 = NSDate()
        for (_,image) in images!.enumerated(){
            dispatchGroup.enter()
            start1 = NSDate()
            uploadFile(image: image){(success,message,imageURL) in
                end1 = NSDate()
                print("Image upload time",end1.timeIntervalSince(start1 as Date))
                if success == true {
                    imageList.append(imageURL!)
                    dispatchGroup.leave()
                    }
                else{
                    completionHandler(false,message)
                    dispatchGroup.leave()
                }
                }
            }
    }
    dispatchGroup.notify(queue: DispatchQueue.main) {
        param["images"] = imageList
        let start1 = NSDate()
        AF.request(URL, method: .post, parameters: param,encoding: JSONEncoding.default, headers: header).validate().responseJSON { response in
            let end1 = NSDate()
            print("Reporting time:",end1.timeIntervalSince(start1 as Date))
            if response.result.isSuccess{
                let json = JSON(response.result.value!)
                if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
                    message = "ServerErrorKey".localized
                    completionHandler(false,message)
                }
                else if json["code"].stringValue == "400"{
                    if json["message"] == "Bad input parameter"{
                        message = "CannotReportKey".localized
                        completionHandler(false,message)
                    }
                }
                else if json["code"].stringValue == "200"{
                    //let data = json["data"]
                    completionHandler(true,nil)
                }
            }
            else {
                if let data = response.data{
                    let err = JSON(data)
                    message = err["message"].string!
                    completionHandler(false,message)
                }
                if let error = response.result.error{
                    if error._code == NSURLErrorTimedOut{
                        message = "RequestTimeOutKey".localized
                        completionHandler(false,message)
                    }
                }
            }
        }
    }
 }
func getTimeReference(completionHandler: @escaping (Bool,String?,Int?) ->()){
    let URL:String = "https://api.bktraffic.com/api/reference/get-all"
    // 2 - create request
    var interval = Int(0)
    var message = ""
    AF.request(URL, method: .get).validate().responseJSON { response in
        if response.result.isSuccess{
            let json = JSON(response.result.value!)
            if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
                message = "ServerErrorKey".localized
                completionHandler(false,message,nil)
            }
            else if json["code"].stringValue == "200"{
                let json = JSON(response.result.value!)
                let data = json["data"]
                for (_,subJson):(String, JSON) in data {
                    if subJson["group"].stringValue == "update-status-interval" {
                        interval = subJson["value"].intValue
                    }
                }
                completionHandler(true,nil,interval)
            }
        }
        else {
            if let data = response.data{
                let err = JSON(data)
                message = err["message"].string!
                completionHandler(false,message,nil)
            }
            if let error = response.result.error{
                if error._code == NSURLErrorTimedOut{
                    message = "RequestTimeOutKey".localized
                    completionHandler(false,message,nil)
                }
            }
        }
    }
}

func getDetailTrafficReport(id:String,completionHandler:@escaping(Bool,String?,String?,String?,String?,String?,[String]?,String?,[String]?) ->()){
    let URL:String = "https://api.bktraffic.com/api/report/segment/report-detail"
    var fullname = String()
    var avatar = String()
    var vel = String()
    var cause = [String]()
    var description = String()
    var images = [String]()
    var userID = String()
    var message = ""
    // 2 - create request
    AF.request(URL, method: .get, parameters: ["id":id]).validate().responseJSON { response in
        if response.result.isSuccess{
            let json = JSON(response.result.value!)
            if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
                message = "ServerErrorKey".localized
                 completionHandler(false,message,nil,nil,nil,nil,nil,nil,nil)
            }
            else if json["code"].stringValue == "200"{
                // result of response serialization
                let json = JSON(response.result.value!)
                let data = json["data"]
                let user = data["user"]
                if !user["name"].stringValue.isEmpty{
                    fullname = user["name"].stringValue
                }
                if !user["avatar"].stringValue.isEmpty{
                    avatar = user["avatar"].stringValue
                }
                if !user["_id"].stringValue.isEmpty{
                    userID = user["_id"].stringValue
                }
                if !data["velocity"].stringValue.isEmpty{
                    vel = data["velocity"].stringValue
                }
                if !data["causes"].isEmpty{
                    let causes = data["causes"].array
                    for i in 0..<causes!.count{
                        cause.append(causes![i].stringValue)
                    }
                }
                if !data["images"].isEmpty{
                    let image = data["images"].array
                    for i in 0..<image!.count{
                        images.append(image![i].stringValue)
                    }
                }
                if !data["description"].stringValue.isEmpty{
                    description = data["description"].stringValue
                }
                completionHandler(true,nil,fullname,avatar,userID,vel,cause,description,images)
            }
        }
        else {
            if let data = response.data{
                let err = JSON(data)
                message = err["message"].string!
                 completionHandler(false,message,nil,nil,nil,nil,nil,nil,nil)
            }
            if let error = response.result.error{
                if error._code == NSURLErrorTimedOut{
                    message = "RequestTimeOutKey".localized
                     completionHandler(false,message,nil,nil,nil,nil,nil,nil,nil)
                }
            }
        }
    }
}
func getCurrentTrafficReport(lat:Double,long:Double,segmentIDs:[String],completionHandler:@escaping(Bool,String?,[CLLocationCoordinate2D:[String]])->()){
    let URL:String = "https://api.bktraffic.com/api/report/segment/current-reports"
    var message = ""

    var ids = [CLLocationCoordinate2D:[String]]()
    // 2 - create request
    AF.request(URL, method: .get, parameters: ["lat":lat,"lng":long])
                .validate()
                .responseJSON { response in
    // 3 - HTTP response handle
                guard response.result.isSuccess else {
                            if let data = response.data{
                      let err = JSON(data)
                                if (err["message"].string != nil){
                            message = err["message"].string!
                        }
                  completionHandler(false,message,ids)
                  }
                return
                  }
                    // result of response serialization
                    let json = JSON(response.result.value!)
                    let data = json["data"]
                    for (_,subJson):(String, JSON) in data {
                        let segment = subJson["segment"]
                        if !segmentIDs.contains(segment["_id"].stringValue) {
                            continue
                        }
                        let centerPoint = subJson["center_point"]
                        let coordinate = centerPoint["coordinates"]
                        let long = coordinate[0].doubleValue
                        let lat = coordinate[1].doubleValue
                        var c = CLLocationCoordinate2D()
                        c.latitude = lat
                        c.longitude = long
                        let id = subJson["_id"].stringValue
                        if ids[c] != nil{
                            ids[c]?.append(id)
                        }
                        else{
                            ids[c] = [id]
                        }
                    }
                    completionHandler(true,nil,ids)
                    }
}
func getOldTrafficReport(date:Int,segmentId:Int,completionHandler:@escaping(Bool,String?,[CLLocationCoordinate2D]?,[String]?)->()){
    let URL:String = "https://api.bktraffic.com/api/report/segment/reports"
    var message = ""
    var coordinates = [CLLocationCoordinate2D]()
    var reportId = [String]()
    // 2 - create request
    AF.request(URL, method: .get, parameters: ["time":date,"segmentId":segmentId])
                .validate()
                .responseJSON { response in
    // 3 - HTTP response handle
                guard response.result.isSuccess else {
                            if let data = response.data{
                      let err = JSON(data)
                                if (err["message"].string != nil){
                            message = err["message"].string!
                        }
                  completionHandler(false,message,nil,nil)
                  }
                return
                  }
                    // result of response serialization
                    let json = JSON(response.result.value!)
                    let data = json["data"]
                    for (_,subJson):(String, JSON) in data {
                        let centerPoint = subJson["center_point"]
                        let coordinate = centerPoint["coordinates"]
                        let long = coordinate[0].doubleValue
                        let lat = coordinate[1].doubleValue
                        var c = CLLocationCoordinate2D()
                        c.latitude = lat
                        c.longitude = long
                        coordinates.append(c)
                        let id = subJson["_id"].stringValue
                        reportId.append(id)
                    }
                      completionHandler(true,nil,coordinates,reportId)
                    }
}
func uploadFile(image: UIImageView,completionHandler:@escaping(Bool,String?,String?) ->()){
    let img = image.image!
    let date = Date()
    var message = ""
    let URL:String = "https://api.bktraffic.com/api/file/upload"
    AF.upload(multipartFormData: { (multipartFormData) in
        var imageData = img.jpegData(compressionQuality: 1)
        print("Actual size:",imageData!.count)
        if imageData!.count > 500000 {
            imageData = img.jpegData(compressionQuality: 0.5)
        }
        multipartFormData.append(imageData!, withName: "file", fileName: "\(date).jpeg", mimeType: "image/jpeg")
        print("Compressed size:",imageData!.count)
    },usingThreshold: UInt64.init(),to: URL,method: .post).validate().responseJSON { response in
        if response.result.isSuccess{
           let json = JSON(response.result.value!)
           if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
               message = "ServerErrorKey".localized
               completionHandler(false,message,nil)
           }
           else if json["code"].stringValue == "200"{
                let json = JSON(response.result.value!)
                let data = json["data"].stringValue
                completionHandler(true,nil,data)
           }
       }
       else {
           if let data = response.data{
               let err = JSON(data)
               message = err["message"].string!
               completionHandler(false,message,nil)
           }
           if let error = response.result.error{
               if error._code == NSURLErrorTimedOut{
                   message = "RequestTimeOutKey".localized
                   completionHandler(false,message,nil)
               }
           }
       }
    }
}

func evaluateReport(accessToken:String,reportId:String,score:Float,completionHandler:@escaping(Bool,String?) ->()){
    let URL:String = "https://api.bktraffic.com/api/evaluation/add"
    let headers: HTTPHeaders = [
        "Authorization": "Bearer \(accessToken)",
        "Accept": "application/json"
    ]
    let param: Parameters
    param = ["report":reportId,"score":score]
    AF.request(URL, method: .post,parameters: param,headers: headers)
                .validate()
                .responseJSON { response in
    // 3 - HTTP response handle
                    guard response.result.isSuccess else {
                        print(response.error.debugDescription)
                        var errorMessage = "General error message"
                        if let data = response.data {
                            let responseJSON = JSON(data)
                            if let message: String = responseJSON["message"].stringValue {
                                if !message.isEmpty {
                                    errorMessage = message
                                }
                            }
                        }

                        completionHandler(false,errorMessage)
                        return
                 }
                    let json = JSON(response.result.value!)
                    print(json)
                    //result of response serialization
                    completionHandler(true,nil)
                }}
func getCurrentTrafficStatus(lat:Double,lng:Double,zoom:Int,date:Int,completionHandler: @escaping (Bool,String?,[Double]?,[Double]?,[Double]?,[Double]?,[String]?,[String]?) ->()){
    var message = ""
    var velocity = [String]()
    var color = [String]()
    var sLat = [Double]()
    var sLong = [Double]()
    var eLat = [Double]()
    var eLong = [Double]()

    let URL:String = "https://api.bktraffic.com/api/traffic-status/get-status"
    let param: Parameters
    param = ["lat":lat,"lng":lng,"zoom":zoom,"date":date]
    AF.request(URL, method: .get,parameters: param).validate().responseJSON { response in
        if response.result.isSuccess{
            let json = JSON(response.result.value!)
            if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
                message = "ServerErrorKey".localized
                completionHandler(false,message,nil,nil,nil,nil,nil,nil)
            }
            else if json["code"].stringValue == "200"{
                let json = JSON(response.result.value!)
                  let data = json["data"]
                
                  //let id = data["segment"].stringValue
                  for (_,subJson):(String, JSON) in data {
                      let c = subJson["color"].stringValue
                      color.append(c)
                      let v  = subJson["velocity"].stringValue
                      velocity.append(v)
                      let poly = subJson["polyline"]
                      //print(poly)
                      let coordinate =  poly["coordinates"]
                      sLat.append(coordinate[0][1].doubleValue)
                      sLong.append(coordinate[0][0].doubleValue)
                      eLat.append(coordinate[1][1].doubleValue)
                      eLong.append(coordinate[1][0].doubleValue)
                  }
                completionHandler(true,message,sLat,sLong,eLat,eLong,color,velocity)
            }
        }
        else {
            if let data = response.data{
                let err = JSON(data)
                message = err["message"].string!
                completionHandler(false,message,nil,nil,nil,nil,nil,nil)
            }
            if let error = response.result.error{
                if error._code == NSURLErrorTimedOut{
                    message = "RequestTimeOutKey".localized
                    completionHandler(false,message,nil,nil,nil,nil,nil,nil)
                }
            }
        }
    }
}
func findNearSegment(lat:Double,lng:Double,completionHandler: @escaping (Bool,String?,[String]?,[Double]?,[Double]?,[Double]?,[Double]?) ->()){
    let URL:String = "https://api.bktraffic.com/api/segment/find-near"
    let param: Parameters
    param = ["lat":lat,"lng":lng]
    var idList = [String]()
    var sLat = [Double]()
    var sLong = [Double]()
    var eLat = [Double]()
    var eLong = [Double]()
    var message = ""
    AF.request(URL, method: .get,parameters: param).validate().responseJSON { response in
        if response.result.isSuccess{
            let json = JSON(response.result.value!)
            if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
                message = "ServerErrorKey".localized
                completionHandler(false,message,nil,nil,nil,nil,nil)
            }
            else if json["code"].stringValue == "200"{
                // result of response serialization
                let json = JSON(response.result.value!)
                let data = json["data"]
                //print("Find Near Segment JSON:",data)
                for (_,subJson):(String, JSON) in data {
                    //Do something you want
                    idList.append(subJson["_id"].stringValue)
                    let polyline = subJson["polyline"]
                    let coordinate = polyline["coordinates"]
                    let startLong = coordinate[0][0].stringValue
                    let startLat = coordinate[0][1].stringValue
                    sLat.append(Double(startLat)!)
                    sLong.append(Double(startLong)!)
                    let endLong = coordinate[1][0].stringValue
                    let endLat = coordinate[1][1].stringValue
                    eLat.append(Double(endLat)!)
                    eLong.append(Double(endLong)!)
                }
                if idList.isEmpty{
                    completionHandler(true,nil,nil,nil,nil,nil,nil)
                }else{
                    completionHandler(true,nil,idList,sLat,sLong,eLat,eLong)
                }
            }
        }
        else {
            if let data = response.data{
                let err = JSON(data)
                message = err["message"].string!
                completionHandler(false,message,nil,nil,nil,nil,nil)
            }
            if let error = response.result.error{
                if error._code == NSURLErrorTimedOut{
                    message = "RequestTimeOutKey".localized
                    completionHandler(false,message,nil,nil,nil,nil,nil)
                }
            }
        }
    }
}

func findMapSegments(slat:Float,slng:Float,elat:Float,elng:Float){
    let URL:String = "https://api.bktraffic.com/segments/map"
    let param: Parameters
    param = ["slat":slat,"slng":slng,"elat":elat,"elng":elng]
    AF.request(URL, method: .get,parameters: param)
                .validate()
                .responseJSON { response in
    // 3 - HTTP response handle
                guard response.result.isSuccess else {
                    print("Error while fetching CurrentUserInfo: \(String(describing: response.result.error?.localizedDescription))")
                return
                    }
                    // result of response serialization
                    let json = JSON(response.result.value!)
                    print("Current User JSON:",json)
                    }
}
func snapToRoad(lat:Double,long:Double,completionHandler:@escaping(Bool,Double?,Double?) -> ()){
    var elong = Double()
    var elat = Double()
    let coordinates = "\(lat),\(long)"
    let url = "https://roads.googleapis.com/v1/snapToRoads?path=\(coordinates)&interpolate=true&key=AIzaSyBy9V-ZUsws7Gf1BoYDacA_RCFq5aUMtLQ"
    let request = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        AF.request(request,method:.post)
                .validate()
                .responseJSON { response in
    // 3 - HTTP response handle
                    switch response.result {
                    case .success:
                            let json = JSON(response.result.value!)
                            elong = json["snappedPoints"][0]["location"]["longitude"].doubleValue
                            elat = json["snappedPoints"][0]["location"]["latitude"].doubleValue
                            completionHandler(true,elat,elong)
                    case .failure:
                            print("Error: \(String(describing: response.result.error?.localizedDescription))")
                            completionHandler(false,nil, nil)
                }
                
    }
}
 func openWeatherMap(lat:Double,long:Double,completionHandler:@escaping(Bool,String?,String?,String?,String?) -> ()){
     let apiKey1 = "f23029fa-02a4-4ad6-b3f1-981281c0ff06"
    let apiKey2 = "451bc25e-0dbb-4ccf-99a8-d1706895a920"
     let url = "https://api.airvisual.com/v2/nearest_city?lat=\(lat)&lon=\(long)&key=\(apiKey1)"
     AF.request(url, method: .get)
                 .validate()
                 .responseJSON { response in
     // 3 - HTTP response handle
                 guard response.result.isSuccess else {
                             if let data = response.data{
                       let err = JSON(data)
                                 if (err["message"].string != nil){
                                     print(err["message"].string as Any)
                         }
                   }
                    completionHandler(false,nil,nil,nil,nil)
                 return
                   }
                     // result of response serialization
                     let json = JSON(response.result.value!)
                     let currentInfo = json["data"]["current"]
                     let pollution = currentInfo["pollution"]
                     let weather = currentInfo["weather"]
                    let aqicn = pollution["aqicn"].stringValue
                    let aqius = pollution["aqius"].stringValue
                    let wic = weather["ic"].stringValue
                    let temperature = weather["tp"].stringValue
                    completionHandler(true,aqicn,aqius,wic,temperature)
                     }
 }
func findRoute(flat:Double,flong:Double,tlat:Double,tlong:Double,type:String,completionHandler:@escaping(Bool,String?,[CLLocationCoordinate2D]?,[CLLocationCoordinate2D]?) -> ()){
    let URL:String = "https://api.bktraffic.com/api/segment/direct"
    var message = ""
    var coords1 = [CLLocationCoordinate2D]()
    var coords2 = [CLLocationCoordinate2D]()
    AF.request(URL, method: .get, parameters: ["slat":flat,"slng":flong,"type":type,"elat":tlat,"elng":tlong]).validate().responseJSON { response in
        if response.result.isSuccess{
            let json = JSON(response.result.value!)
            if json["code"].stringValue == "500" || json["code"].stringValue == "503"{
                message = "ServerErrorKey".localized
                completionHandler(false,message,nil,nil)
            }
            else if json["code"].stringValue == "200"{
                let data = json["data"]
                print("Distance:",data[0]["distance"].stringValue)
                let coordArr1 = data[0]["coords"]
                for (_,subJson):(String, JSON) in coordArr1 {
                    coords1.append(CLLocationCoordinate2D(latitude: subJson["lat"].doubleValue, longitude: subJson["lng"].doubleValue))
                }
                let coordArr2 = data[1]["coords"]
                for (_,subJson):(String, JSON) in coordArr2 {
                    coords2.append(CLLocationCoordinate2D(latitude: subJson["lat"].doubleValue, longitude: subJson["lng"].doubleValue))
                }

                completionHandler(true,nil,coords1,coords2)
            }
        }
        else {
            if let data = response.data{
                let err = JSON(data)
                message = err["message"].string!
                completionHandler(false,message,nil,nil)
            }
        }
    }
    
}





